//
//  GIChartView.h
//  GICTestApp
//
//  Created by Danila Parhomenko on 5/8/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "CSView.h"
#import "GIStompDispatcher.h"

@interface GIChartView : CSView <GIStompDispatcherProtocol> {    
    //    NSUInteger updateCounter;
}

- (void) setDispatcher:(GIStompDispatcher *)aDispatcher;
- (void) subscribeTicker:(NSString *)ticker;
- (void) unsubscribe;

- (void) addSubordinateTicker:(NSString *)ticker;
- (void) unsubscribeSubordinateTicker:(NSString *)ticker;

@end
