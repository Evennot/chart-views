Required frameworks
-------------------

Accelerate, QuartzCore

* * *
Usage
-----

###Initialization

    (GIChartView *)self.chartView  
    self.chartView.decimals = 2; // setting default decimals  
    [self.chartView setDispatcher: _dispatcher]; // setting up dispatcher*  
    [self.chartView subscribeTicker:self.ticker]; // selecting ticker and that's all


###Interval change
swiping for interval change:  

    - (IBAction)chartSwiped:(UISwipeGestureRecognizer *)sender {
        if (sender.direction & UISwipeGestureRecognizerDirectionLeft) {
            if (self.chartView.candleItemInterval > ciMinute)
                self.chartView.candleItemInterval = self.chartView.candleItemInterval - 1;
        }
        if (sender.direction & UISwipeGestureRecognizerDirectionRight) {
            if (self.chartView.candleItemInterval < ciWeek)
                self.chartView.candleItemInterval = self.chartView.candleItemInterval + 1;
        }
    }    
###Rotating to pseudo landscape/portrait mode
 In case of initial portrait orientation:
 
        [self.chartView rotate:M_PI Frame:CGRectMake(0, 20, 160, 260)]; // rotating to pseudo landscape
        
###Delegate methods
Required:
        
        - (void) userPinched:(double) zoom Direction:(PinchDirection) dir;
        
Zoom variable is between 0 (minimum candle size) and ~1 (maximum item size)
Optional (in case **hasCaption** property is true :
        
        // captionRectChanged: should return name for this chart that fits in current caption rectangle
        // mainChartColor is the color of main chart. Pretty self explanatory
        - (NSString *) captionRectChanged:(CGRect) newRect Color:(UIColor *)mainChartColor;
        
        - (void) captionWillDisappear; // called when user marks something on chart
        - (void) captionWillAppear; // called when when user mark disappears and normal caption comes back
        // if delegate implements following method, chart will display last price and it's change
        // in the cpation (if visible)
        - (void) getLast:(out double*)last Change:(out double *)change;


###Properties
 * Display prices as percents of last close: *chartView.percentMode*
 * Line/candles mode: *chartView.percentMode*
 * Labels' font *chartView.labelFont*. Default is size 10 system font
 * Right axis width: *chartView.verticalAxisWidth*. Default is 70
 * Time axis height: *chartView.horizontalAxisHeight*. Default is 20
 * Loading animation: *chartView.animated*
 * Width candles or close line stroke: *chartView.lineWidth*
 * Main price/volume color: *chartView.lineColor*
 * Vertical grid color: *chartView.verticalGridColor* and width: *chartView.verticalGridWidth*
 * Horizontal grid color: *chartView.horizontalGridColor* and width: *chartView.horizontalGridWidth*
 * Show vertical stripes for big periods (years/months, etc) *chartView.showVerticalStripes* and color for them:*chartView.stripeColor*
 