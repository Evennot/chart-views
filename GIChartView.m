//
//  GIChartView.m
//  GICTestApp
//
//  Created by Danila Parhomenko on 5/8/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "GIChartView.h"

@interface GIChartView () {
    NSUInteger scaleIncId;
}

@property (nonatomic, weak)   GIStompDispatcher *dispatcher;
@property (nonatomic, strong) GIStompCandles    *candlesFrame;

@property NSString *ticker;
@property NSString *market;
#ifdef mywarns
#warning ^^ actually not used...
#endif
@property NSString *tickerName;

@property (nonatomic, strong) NSMutableDictionary *subTickers;

@end

@implementation GIChartView

- (void) setDispatcher:(GIStompDispatcher *)aDispatcher{
    _dispatcher = aDispatcher;
}

NSString *intervalName(CandleInterval i) {
    switch (i) {
        case ciMinute: return @"M1";
        case ci10Minutes: return @"M10";
        case ciHour: return @"H1";
        case ciDay: return @"D1";
        case ciWeek: return @"D7";
        case ciMonth: return @"m1";
        default: return @"M10";
    }
}

- (void) subscribeTicker:(NSString *)ticker{
    
    self.ticker = ticker;
    self.market = [self.ticker componentsSeparatedByString:@"."][0];
    self.tickerName = [self.ticker componentsSeparatedByString:@"."][2];
    
    //self.candlesFrame = [GIStompCandles select:self.ticker andInterval:@"M10"];
    
    self.candleItemInterval = ci10Minutes;
    //[_dispatcher registerMessage:self.candlesFrame];
}

- (void) unsubscribe {
    if (self.candlesFrame)
        [_dispatcher unregisterMessage:self.candlesFrame];
}

- (NSMutableDictionary *) makeTickerDict:(NSString *) ticker {
    NSString *xname = [ticker componentsSeparatedByString:@"."][2];
    GIStompCandles *xframe = [GIStompCandles select:ticker andInterval:intervalName(self.candleItemInterval)];
    [_dispatcher registerMessage:xframe];
    xframe.delegate = self;
   return [@{@"name": xname, @"scaleId": @(scaleIncId++), @"frame": xframe} mutableCopy];
}

- (void) addSubordinateTicker:(NSString *)ticker {
    if (self.subTickers) {
        if ([self.subTickers valueForKey:ticker])
            [self unsubscribeSubordinateTicker:ticker];
        [self.subTickers setValue:[self makeTickerDict:ticker] forKey:ticker];
    } else
        self.subTickers = [@{ticker: [self makeTickerDict:ticker]} mutableCopy];
}

- (void) unsubscribeSubordinateTicker:(NSString *)ticker {
    if (self.subTickers) {
        NSDictionary *d = [self.subTickers valueForKey:ticker];
        if (d) {
            [_dispatcher unregisterMessage:[d valueForKey:@"frame"]];
            [self.subTickers setValue:nil forKey:ticker];
        }
    }
}

#ifdef EMUCACHE
- (NSString *) applicationDocumentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}
#endif

- (void) didFrameReceive:(id)frame {
    dispatch_async(dispatch_get_main_queue(), ^(void){
        if ([frame isKindOfClass:[GIStompCandles class]]) {
            GIStompMessage *m = frame;
            if ([self.candlesFrame.subscriptionID isEqualToNumber:m.subscriptionID]) { // main chart
                for (GICommonStompFrame *c = [m.responseQueue pop]; c ; c = [m.responseQueue pop])
                    if (c.body.properties) {
                        if (c.body.properties.type == GISTOMP_SNAPSHOT)
                            [self resetData];
                    
                        [self feedData:c.body.data WithColumns:c.body.columns SubsId:@(-1) Main:YES];
                    } else {
                        NSLog(@"Bad candles: %@", c.body);
                        [self resetData];
                        //        NSLog(@" Queue length = %i %@ %@ : %@", [m.responseQueue size], [m.lastResponse.body stringForIndex:0 andKey:@"TICKER"], m.lastResponse.body.properties, [c description]);
                    }
            } else {
                [self.subTickers enumerateKeysAndObjectsUsingBlock:^(id ticker, NSDictionary *d, BOOL *stop) {
                    GIStompCandles *f = [d valueForKey:@"frame"];
                    if ([f.subscriptionID isEqualToNumber:m.subscriptionID]) {
                        NSNumber *scaleId = [d valueForKey:@"scaleId"];
                        *stop = YES;
                        for (GICommonStompFrame *c = [m.responseQueue pop]; c ; c = [m.responseQueue pop])
                            if (c.body.properties) {
                                if (c.body.properties.type == GISTOMP_SNAPSHOT)
                                    [self resetSubScaleData:scaleId];
                                
                                [self feedData:c.body.data WithColumns:c.body.columns SubsId:scaleId Main:NO];
                            } else {
                                NSLog(@"Bad candles: %@", c.body);
                                [self resetSubScaleData:scaleId];
                            }
                    }
                }];
            }
        }
    });
}

#ifdef EMUCACHE
- (void) doSimulteDataComming {
    [self resetData];
    NSString *fileName = [NSString stringWithFormat:@"%@/%@%d%@.data", [self applicationDocumentsDirectory], self.tickerName, self.candleItemInterval, @"snap"];
    NSLog(@"%@", fileName);
    NSDictionary *x = [NSDictionary dictionaryWithContentsOfFile:fileName];
    [self feedData:[x valueForKey:@"data"] WithColumns:[x valueForKey:@"columns"] SubsId:@0 Main:YES];
}

- (void) doSimulteSubDataComming:(NSNumber *)x {
    //[self resetData];
    NSString *fileName = [NSString stringWithFormat:@"%@/%@%d%@.data", [self applicationDocumentsDirectory], [x isEqualToNumber:@1]?@"TGKA":@"FEES", self.candleItemInterval, @"snap"];
    NSLog(@"%@", fileName);
    NSDictionary *xx = [NSDictionary dictionaryWithContentsOfFile:fileName];
    [self feedData:[xx valueForKey:@"data"] WithColumns:[xx valueForKey:@"columns"] SubsId:x Main:NO];
}

- (void) simulateDataComming {
    [self performSelectorOnMainThread:@selector(doSimulteDataComming) withObject:nil waitUntilDone:YES];
}

- (void) simulateSubDataComming:(NSNumber *)x {
    [self performSelectorOnMainThread:@selector(doSimulteSubDataComming:) withObject:x waitUntilDone:YES];
}
#endif
#ifdef mywarns
#warning PUSH IT ALL TO GICFIX
#endif

- (void) candleIntervalChanged {
    if (self.candlesFrame)
        [_dispatcher unregisterMessage:self.candlesFrame];
    if (self.subTickers) {
        [self.subTickers enumerateKeysAndObjectsUsingBlock:^(id ticker, NSMutableDictionary *d, BOOL *stop) {
            GIStompCandles *f = [d valueForKey:@"frame"];
            [_dispatcher unregisterMessage:f];
            f.delegate = nil;
        }];
    }
    [super candleIntervalChanged];

    self.candlesFrame = [GIStompCandles select:self.ticker andInterval:intervalName(self.candleItemInterval)];
    _candlesFrame.delegate = self;
    [_dispatcher registerMessage:self.candlesFrame];
#ifdef EMUCACHE
#warning temporary
    [self performSelector:@selector(simulateDataComming) withObject:nil afterDelay:1.0];
    [self performSelector:@selector(simulateSubDataComming:) withObject:@1 afterDelay:2.0];
#warning temporary
#endif
    
    if (self.subTickers) {
        [self.subTickers enumerateKeysAndObjectsUsingBlock:^(id ticker, NSMutableDictionary *d, BOOL *stop) {
            GIStompCandles *f = [GIStompCandles select:ticker andInterval:intervalName(self.candleItemInterval)];
            [d setValue:f forKey:@"frame"];
            f.delegate = self;
            [_dispatcher registerMessage:f];
        }];
    }

}

@end
