//
//  CSCandleRepresentationCandles.m
//  chartster
//
//  Created by Danila Parhomenko on 4/17/13.
//  Copyright (c) 2013 Itheme. All rights reserved.
//

#import "CSCandleRepresentationCandles.h"
#import <Accelerate/Accelerate.h>

@implementation CSCandleRepresentationCandles

- (void) dealloc {
    if (lineCapacity) {
        free(placeholder);
        free(tmp);
    }
}

- (void) useMainColorOn:(CGContextRef) context {
    CGContextSetRGBFillColor(context, mainR, mainG, mainB, mainAlpha);
    CGContextSetRGBStrokeColor(context, mainR, mainG, mainB, mainAlpha);
    CGContextSetLineWidth(context, [self.stickWidth floatValue]);
}

- (void) setLineLength:(int) theLineLength HLOCVs:(HLOCV *)hlocs ItemSize:(CGFloat)curItemSize RepresentationFrame:(CGRect) rFrame VolumeFrame:(CGRect)rVolume {
    lineLength = theLineLength;
    if (lineCapacity < lineLength) {
        if (placeholder) free(placeholder);
        placeholder = malloc(sizeof(CGPoint) * lineLength * 18);
        CGPoint *ptr = placeholder;
        highsLows = ptr; ptr += lineLength * 4;
        candleBodiesRise = (CGRect *)ptr; ptr += lineLength * 2;
        candleBodiesFall = (CGRect *)ptr; ptr += lineLength * 2;
        candleBodiesSame = ptr; ptr += lineLength * 2;
        animHighsLows = ptr; ptr += lineLength * 2;
        animCandleBodiesRise = (CGRect *)ptr; ptr += lineLength * 2;
        animCandleBodiesFall = (CGRect *)ptr; ptr += lineLength * 2;
        animCandleBodiesSame = ptr; //ptr += lineLength * 2;
        if (tmp) free(tmp);
        tmp = malloc(sizeof(float) * lineLength * 2);

        lineCapacity = lineLength;
    }
    riseCount = fallCount = sameCount = highsLowsCount = 0;
    float startX = rFrame.origin.x;
    float itemSize = curItemSize;
    float dItem = itemSize / 3.0;
    float *startPtr = &(hlocs->high);
    vDSP_maxv(startPtr, HLOCVStride, &maxP, lineLength);
    startPtr = &(hlocs->low);
    vDSP_minv(startPtr, HLOCVStride, &minP, lineLength);
    float margin = (maxP - minP) * 0.1; // 10% margin above max and below min
    maxP += margin;
    minP -= margin;
    startPtr = &(hlocs->value);
    vDSP_maxv(startPtr, HLOCVStride, &maxValue, lineLength);
    maxValue *= 1.10; // 10% margin
    /*vDSP_vramp(&(startX), &(itemSize), &(highsLows[0].x), 4, lineLength * 2);
    vDSP_vramp(&(startX), &(itemSize), &(highsLows[1].x), 4, lineLength * 2);
    startX = rFrame.origin.x - (itemSize / 3.0);
    vDSP_vramp(&(startX), &(itemSize), &(candleBodiesRise[0].origin.x), 4, lineLength);
    vDSP_vramp(&(startX), &(itemSize), &(candleBodiesFall[0].origin.x), 4, lineLength);
    vDSP_vramp(&(startX), &(itemSize), &(candleBodiesSame[0].x), 4, lineLength);
    startX = rFrame.origin.x + (itemSize / 3.0);
    vDSP_vramp(&(startX), &(itemSize), &(candleBodiesSame[1].x), 4, lineLength);

    float candleWidth = itemSize * 2.0/3.0;
    vDSP_vfill(&candleWidth, &(candleBodiesRise[0].size.width), 4, lineLength * 2); // + candleBodiesFall
*/    
    [super setLineLength:theLineLength HLOCVs:hlocs ItemSize:curItemSize RepresentationFrame:rFrame VolumeFrame:rVolume];
    float x0;
    float x1;
    for (int i = 0; i < lineLength; i++) {
        if (hlocs[i].value > 0) {
            float x = startX + i * itemSize;
            x0 = x - dItem;
            x1 = x + dItem;
            float h = hlocs[i].high;
            float l = hlocs[i].low;
            float o = hlocs[i].open;
            float c = hlocs[i].close;
            float d = o - c;
            if (d > EPSILON) {              // h >= o > c >= l
                if (h - o > EPSILON) {      // h > o
                    highsLows[highsLowsCount++] = CGPointMake(x, h);
                    if (c - l > EPSILON)    // h > o > c > l
                        highsLows[highsLowsCount++] = CGPointMake(x, l);
                    else                    // h > o > c = l
                        highsLows[highsLowsCount++] = CGPointMake(x, o);
                } else {                    // h = o
                    if (c - l > EPSILON) {  // h = o > c > l
                        highsLows[highsLowsCount++] = CGPointMake(x, c);
                        highsLows[highsLowsCount++] = CGPointMake(x, l);
                    } // else                  h = o > c = l
                }
                candleBodiesFall[fallCount++] = CGRectMake(x0, o, x1 - x0, d);
            } else {
                if (d > -EPSILON) {         // h >= o = c >= l
                    candleBodiesSame[sameCount++] = CGPointMake(x0, o);
                    candleBodiesSame[sameCount++] = CGPointMake(x1, c);
                    if (h - l > EPSILON) {
                        highsLows[highsLowsCount++] = CGPointMake(x, h);
                        highsLows[highsLowsCount++] = CGPointMake(x, l);
                    }
                } else {                    // h >= c > o >= l
                    if (h - c > EPSILON) {  // h > c
                        highsLows[highsLowsCount++] = CGPointMake(x, h);
                        highsLows[highsLowsCount++] = CGPointMake(x, c);
                    }
                    candleBodiesRise[riseCount++] = CGRectMake(x0, c, x1 - x0, -d);
                    if (o - l > EPSILON) {  // o > l
                        highsLows[highsLowsCount++] = CGPointMake(x, o);
                        highsLows[highsLowsCount++] = CGPointMake(x, l);
                    }
                }
            }
        }
    }
    startPtr = &(candleBodiesRise[0].size.height);
    float resc = -rescale;
    vDSP_vsmul(startPtr, 4, &resc, startPtr, 4, lineCapacity*2); // rises and falls rects heights
    if (highsLowsCount) {
        startPtr = &(highsLows[0].y);
        vDSP_vsmsa(startPtr, 2, &rescale, &offset, startPtr, 2, highsLowsCount);
    }
    if (fallCount > 0) {
        startPtr = &(candleBodiesFall[0].origin.y);
        vDSP_vsmsa(startPtr, 4, &rescale, &offset, startPtr, 4, fallCount);
    }
    if (riseCount > 0) {
        startPtr = &(candleBodiesRise[0].origin.y);
        vDSP_vsmsa(startPtr, 4, &rescale, &offset, startPtr, 4, riseCount);
    }
    if (sameCount > 0) {
        startPtr = &(candleBodiesSame[0].y);
        vDSP_vsmsa(startPtr, 2, &rescale, &offset, startPtr, 2, sameCount);
    }
}

- (void) drawPresentationInContext:(CGContextRef) context Drawen:(float) drawen Addendum:(float) add {
    [self useMainColorOn:context];
    CGContextSaveGState(context);
    CGRect clipRect = priceFrame;
    if (clipRect.origin.x > 0) {
        clipRect.size.width += clipRect.origin.x;
        clipRect.origin.x = 0;
    }
    CGContextClipToRect(context, clipRect);
    if (highsLowsCount > 0) {
        CGPoint *p = highsLows;
        for (int i = highsLowsCount * drawen / 2; i--; p++) {
            CGContextMoveToPoint(context, p->x, p->y);
            p++;
            CGContextAddLineToPoint(context, p->x, p->y);
        }
    }
    if (sameCount > 0) {
        CGPoint *p = candleBodiesSame;
        for (int i = sameCount * drawen / 2; i--; p++) {
            CGContextMoveToPoint(context, p->x, p->y);
            p++;
            CGContextAddLineToPoint(context, p->x, p->y);
        }
    }
    if (riseCount > 0) {
        CGContextAddRects(context, candleBodiesRise, riseCount * drawen);
    }
    CGContextStrokePath(context);
    if (fallCount > 0) {
        CGContextAddRects(context, candleBodiesFall, fallCount * drawen);
        //CGContextSetRGBFillColor(context, 0, 0, 0, 1.0);
        CGContextFillPath(context);
    }
    CGContextRestoreGState(context);
    [super drawPresentationInContext:context Drawen:drawen Addendum:add];
}

- (void) drawInContext:(CGContextRef)context {
    [self useMainColorOn:context];
    CGContextSaveGState(context);
    CGContextClipToRect(context, priceFrame);
    if (highsLowsCount > 0) {
        CGPoint *p = highsLows;
        for (int i = highsLowsCount / 2; i--; p++) {
            CGContextMoveToPoint(context, p->x, p->y);
            p++;
            CGContextAddLineToPoint(context, p->x, p->y);
        }
    }
    if (sameCount > 0) {
        CGPoint *p = candleBodiesSame;
        for (int i = sameCount / 2; i--; p++) {
            CGContextMoveToPoint(context, p->x, p->y);
            p++;
            CGContextAddLineToPoint(context, p->x, p->y);
        }
    }
    if (riseCount > 0) {
        CGContextAddRects(context, candleBodiesRise, riseCount);
    }
    CGContextStrokePath(context);
    if (fallCount > 0) {
        CGContextAddRects(context, candleBodiesFall, fallCount);
        //CGContextSetRGBFillColor(context, 0, 0, 0, 1.0);
        CGContextFillPath(context);
    }
    CGContextRestoreGState(context);

    [super drawInContext:context];
}

@end
