//
//  CSScaleRepresentationBars.m
//  GICTestApp
//
//  Created by Danila Parhomenko on 6/4/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "CSScaleRepresentationBars.h"
#import <Accelerate/Accelerate.h>

@interface CSScaleRepresentationBars () {
}

@end

@implementation CSScaleRepresentationBars

- (void) dealloc {
    if (lineCapacity) {
        free(placeholder);
        free(tmp);
    }
}

- (void) useMainColorOn:(CGContextRef) context {
    CGContextSetRGBFillColor(context, mainR, mainG, mainB, mainAlpha);
    CGContextSetRGBStrokeColor(context, mainR, mainG, mainB, mainAlpha);
    CGContextSetLineWidth(context, [self.stickWidth floatValue]);
}

- (void) setLineLength:(int) theLineLength HLOCVs:(HLOCV *)hlocs ItemSize:(CGFloat)curItemSize RepresentationFrame:(CGRect) rFrame VolumeFrame:(CGRect)rVolume {
    lineLength = theLineLength;
    if (lineCapacity < lineLength) {
        if (placeholder) free(placeholder);
        placeholder = malloc(sizeof(CGPoint) * lineLength * 18);
        CGPoint *ptr = placeholder;
        barBodiesRise = ptr; ptr += lineLength * 6;
        barBodiesFall = ptr; ptr += lineLength * 6;
        barBodiesSame = ptr;
        /*barBodiesRise = (CGPoint *)ptr; ptr += lineLength;
        barBodiesFall = (CGPoint *)ptr; ptr += lineLength;
        barBodiesSame = ptr; ptr += lineLength * 2;
        animHighsLows = ptr; ptr += lineLength * 2;
        animCandleBodiesRise = (CGRect *)ptr; ptr += lineLength * 2;
        animCandleBodiesFall = (CGRect *)ptr; ptr += lineLength * 2;
        animCandleBodiesSame = ptr; //ptr += lineLength * 2;*/
        if (tmp) free(tmp);
        tmp = malloc(sizeof(float) * lineLength * 2);
        
        lineCapacity = lineLength;
    }
    riseCount = fallCount = sameCount = 0;
    float startX = rFrame.origin.x;
    float itemSize = curItemSize;
    float dItem = itemSize / 3.0;
    float *startPtr = &(hlocs->high);
    vDSP_maxv(startPtr, HLOCVStride, &maxP, lineLength);
    startPtr = &(hlocs->low);
    vDSP_minv(startPtr, HLOCVStride, &minP, lineLength);
    float margin = (maxP - minP) * 0.10; // 10% margin above max and below min
    maxP += margin;
    minP -= margin;
    startPtr = &(hlocs->value);
    vDSP_maxv(startPtr, HLOCVStride, &maxValue, lineLength);
    maxValue *= 1.10; // 10% margin
    [super setLineLength:theLineLength HLOCVs:hlocs ItemSize:curItemSize RepresentationFrame:rFrame VolumeFrame:rVolume];
    float x0;
    float x1;
    for (int i = 0; i < lineLength; i++) {
        if (hlocs[i].value > 0) {
            float x = startX + i * itemSize;
            x0 = x - dItem;
            x1 = x + dItem;
            float h = hlocs[i].high;
            float l = hlocs[i].low;
            float d = h - l;
            if (d > EPSILON) { // h > l
                float o = hlocs[i].open;
                float c = hlocs[i].close;
                d = o - c;
                if (d > EPSILON) { // falling bar
                    barBodiesFall[fallCount++] = CGPointMake(x0, o);
                    barBodiesFall[fallCount++] = CGPointMake(x, o);
                    barBodiesFall[fallCount++] = CGPointMake(x, c);
                    barBodiesFall[fallCount++] = CGPointMake(x1, c);

                    barBodiesFall[fallCount++] = CGPointMake(x, h);
                    barBodiesFall[fallCount++] = CGPointMake(x, l);
                } else {
                    if (d > - EPSILON) { // same price bar
                        barBodiesSame[sameCount++] = CGPointMake(x0, o);
                        barBodiesSame[sameCount++] = CGPointMake(x0, c);
                        
                        barBodiesSame[sameCount++] = CGPointMake(x, h);
                        barBodiesSame[sameCount++] = CGPointMake(x, l);
                    } else { // rising bar
                        barBodiesRise[riseCount++] = CGPointMake(x0, o);
                        barBodiesRise[riseCount++] = CGPointMake(x, o);
                        barBodiesRise[riseCount++] = CGPointMake(x, c);
                        barBodiesRise[riseCount++] = CGPointMake(x1, c);
                        
                        barBodiesRise[riseCount++] = CGPointMake(x, h);
                        barBodiesRise[riseCount++] = CGPointMake(x, l);
                    }
                }
            } else {
                barBodiesSame[sameCount++] = CGPointMake(x0, h);
                barBodiesSame[sameCount++] = CGPointMake(x0, h);
            }
        }
    }
    startPtr = &(barBodiesRise[0].y);
    if (riseCount)
        vDSP_vsmsa(startPtr, 2, &rescale, &offset, startPtr, 2, riseCount);
    if (fallCount) {
        startPtr = &(barBodiesFall[0].y);
        vDSP_vsmsa(startPtr, 2, &rescale, &offset, startPtr, 2, fallCount);
    }
    if (sameCount) {
        startPtr = &(barBodiesSame[0].y);
        vDSP_vsmsa(startPtr, 2, &rescale, &offset, startPtr, 2, sameCount);
    }
}

- (void) drawPresentationInContext:(CGContextRef) context Drawen:(float) drawen Addendum:(float) add {
    //    vDSP_vsmsa(&(line[0].y), 2, &drawen, &add, &(animline[0].y), 2, lineLength * drawen);
    //    CGContextAddLines(context, animline, lineLength * drawen);
    //    CGContextStrokePath(context);
    [self useMainColorOn:context];
    CGContextSaveGState(context);
    CGContextClipToRect(context, priceFrame);
    if (riseCount > 0) {
        CGPoint *p = barBodiesRise;
        for (int i = riseCount * drawen / 2; i--; p++) {
            CGContextMoveToPoint(context, p->x, p->y);
            p++;
            CGContextAddLineToPoint(context, p->x, p->y);
        }
    }
    if (fallCount > 0) {
        CGPoint *p = barBodiesFall;
        for (int i = fallCount * drawen / 2; i--; p++) {
            CGContextMoveToPoint(context, p->x, p->y);
            p++;
            CGContextAddLineToPoint(context, p->x, p->y);
        }
    }
    if (sameCount > 0) {
        CGPoint *p = barBodiesSame;
        for (int i = sameCount * drawen / 2; i--; p++) {
            CGContextMoveToPoint(context, p->x, p->y);
            p++;
            CGContextAddLineToPoint(context, p->x, p->y);
        }
    }
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
    //    if (lastItemSize < 6.0) {
    //        CGContextSetLineWidth(context, lastItemSize / 3.0);
    //    }
    //    CGContextSetRGBStrokeColor(context, 0, 0, 0, 1.0);
    [super drawPresentationInContext:context Drawen:drawen Addendum:add];
}

- (void) drawInContext:(CGContextRef)context {
    [self useMainColorOn:context];
    CGContextSaveGState(context);
    CGContextClipToRect(context, priceFrame);
    if (riseCount > 0) {
        CGPoint *p = barBodiesRise;
        for (int i = riseCount / 2; i--; p++) {
            CGContextMoveToPoint(context, p->x, p->y);
            p++;
            CGContextAddLineToPoint(context, p->x, p->y);
        }
    }
    if (fallCount > 0) {
        CGPoint *p = barBodiesFall;
        for (int i = fallCount / 2; i--; p++) {
            CGContextMoveToPoint(context, p->x, p->y);
            p++;
            CGContextAddLineToPoint(context, p->x, p->y);
        }
    }
    if (sameCount > 0) {
        CGPoint *p = barBodiesSame;
        for (int i = sameCount / 2; i--; p++) {
            CGContextMoveToPoint(context, p->x, p->y);
            p++;
            CGContextAddLineToPoint(context, p->x, p->y);
        }
    }
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
    [super drawInContext:context];
}


@end
