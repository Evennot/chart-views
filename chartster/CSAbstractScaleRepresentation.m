//
//  CSAbstractScaleRepresentation.m
//  chartster
//
//  Created by Danila Parhomenko on 4/17/13.
//  Copyright (c) 2013 Itheme. All rights reserved.
//

#import "CSAbstractScaleRepresentation.h"
#import <Accelerate/Accelerate.h>

@interface CSAbstractScaleRepresentation ()


@end

@implementation CSAbstractScaleRepresentation

- (id) initWithScaler:(id<SmoothScalerProtocol>) sc {
    self = [super init];
    if (self) {
        self.scaler = sc;
    }
    return self;
}

- (void) setLineLength:(int) theLineLength HLOCVs:(HLOCV *)hlocs ItemSize:(CGFloat)curItemSize RepresentationFrame:(CGRect) rFrame VolumeFrame:(CGRect) rVolume {
    priceFrame = rFrame;
    volumeFrame = rVolume;
    rescale = - (rFrame.size.height);
    if (maxP - minP > 0.000001) {
        if (_scaler)
            [_scaler correctMax:&maxP Min:&minP];
        rescale /= (maxP - minP);
    }
    offset = - maxP*rescale + rFrame.origin.y;
    volumeRescale = - (rVolume.size.height);
    if (maxValue > 0.000001)
        volumeRescale /= maxValue;
    volumeOffset = - maxValue*volumeRescale + rVolume.origin.y;
    
    if (volumeCapacity < theLineLength) {
        if (volumeBodies) {
            free(volumeBodies);
            free(animVolumeBodies);
        }
        volumeBodies = malloc(sizeof(CGRect)*theLineLength);
        animVolumeBodies = malloc(sizeof(CGRect)*theLineLength);
        volumeCapacity = theLineLength;
    }
    volumeCount = theLineLength;
    float itemSize = curItemSize;
    float dItemSize = itemSize / 3;
    float theX = rFrame.origin.x - dItemSize;
    dItemSize *= 2;
    vDSP_vfill(&dItemSize, &(volumeBodies[0].size.width), 4, volumeCount);
    int j = 0;
    for (int i = 0; i < volumeCount; i++) {
        if (hlocs[i].value > EPSILON) {
            volumeBodies[j].origin = CGPointMake(theX, hlocs[i].value);
            volumeBodies[j++].size.height = - hlocs[i].value;
        }
        theX += itemSize;
    }
    volumeCount = j;
    vDSP_vsmsa(&(volumeBodies[0].origin.y), 4, &volumeRescale, &volumeOffset, &(volumeBodies[0].origin.y), 4, volumeCount);
    vDSP_vsmul(&(volumeBodies[0].size.height), 4, &volumeRescale, &(volumeBodies[0].size.height), 4, volumeCount);
    memcpy(animVolumeBodies, volumeBodies, sizeof(CGRect)*volumeCount);
}
    
- (void) dealloc {
    if (volumeBodies) {
        free(volumeBodies);
        free(animVolumeBodies);
    }
}

- (void) useMainColorOn:(CGContextRef) context {
    CGContextSetRGBFillColor(context, mainR, mainG, mainB, mainAlpha);
    CGContextSetRGBStrokeColor(context, mainR, mainG, mainB, mainAlpha);
    CGContextSetLineWidth(context, [_lineWidth floatValue]);
}

- (void) drawPresentationInContext:(CGContextRef) context Drawen:(float) drawen Addendum:(float) add {
    CGContextSaveGState(context);
    CGContextClipToRect(context, volumeFrame);
    vDSP_vsmsa(&(volumeBodies[0].origin.y), 4, &drawen, &add, &(animVolumeBodies[0].origin.y), 4, volumeCount * drawen);
    CGContextAddRects(context, animVolumeBodies, volumeCount * drawen);
    CGContextFillPath(context);
    CGContextRestoreGState(context);

//    @throw [NSException exceptionWithName:@"abstract scale representation used" reason:nil userInfo:nil];
}

- (void) drawInContext:(CGContextRef)context {    
    CGContextSaveGState(context);
    CGContextClipToRect(context, volumeFrame);
    CGContextAddRects(context, volumeBodies, volumeCount);
    CGContextFillPath(context);
    CGContextRestoreGState(context);

//    @throw [NSException exceptionWithName:@"abstract scale representation used" reason:nil userInfo:nil];
}

- (double) priceAtY:(float) y {
    double res = y / rescale;
    res -= offset / rescale;
    return res;
}

- (float) firstX {
    if (volumeCount > 0)
        return volumeBodies[0].origin.x;
    return 0;
}

- (float) lastX {
    if (volumeCount > 0)
        return volumeBodies[volumeCount - 1].origin.x + volumeBodies[volumeCount - 1].size.width;
    return 0;
}

- (float) yForPrice:(double) p {
    return p*rescale + offset;
}

- (void) setMainColor:(UIColor *)mc {
    _mainColor = mc;
    [mc getRed:&mainR green:&mainG blue:&mainB alpha:&mainAlpha];
}

@end
