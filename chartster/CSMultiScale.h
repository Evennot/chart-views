//
//  CSMultiScale.h
//  GICTestApp
//
//  Created by Danila Parhomenko on 6/6/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "CSAbstractScale.h"
#import "CSSubContainer.h"
#import "CSScale.h"

@interface CSMultiScale : CSAbstractScale {
    //HLOCV **hlocs;
    //CFAbsoluteTime *times;
    //NSUInteger *count;
    //NSUInteger *capacity;
    CGFloat desiredItemSize;
    
    //CandleInterval candleItemInterval;
    
}

//@property (nonatomic, readonly, strong) CSSubContainer *feededPacket;

- (id) initFor:(CandleInterval)theCandleItemInterval;
- (id) initAsExtensionOf:(CSScale *) singleScale NewScaleId:(NSString *) scaleId DecFormat:(NSString *)decimalledFormat;
- (CSSubContainer *) beginFeedingSubScaleId:(NSString *)scaleId Main:(BOOL) isMain DecFormat:(NSString *)decimalledFormat;
- (void) applyAttributes:(NSArray *) attrs;
- (void) recalcViewInFrame:(CGRect)intFrame ClippingInset:(CGFloat)cInset;

@end
