//
//  GICAbstractChartView.h
//  GICTestApp
//
//  Created by Danila Parhomenko on 5/24/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSEssentials.h"

@interface GICAbstractChartView : UIView
#ifdef mywarns
#warning ^^ not used
#endif


@end
