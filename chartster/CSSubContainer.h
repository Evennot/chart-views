//
//  CSSubContainer.h
//  GIConnector
//
//  Created by Danila Parhomenko on 6/6/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSEssentials.h"

typedef enum MergeResultTag {
    mrGenericBad = 0,
    // --sub--
    //          --main--
    mrCantMergeYetB = 1, // subdata is all earlier than main
    //           --sub--
    // --main--
    mrCantMergeYetA = 2, // subdata is all after main
    // ??---sub---
    //    --main--
    mrIdeal = 3, // subdata presents till last candle of main data
    // --sub--
    //     --main--
    mrMergeOverlappedPre = 4,
    // ??------sub------
    //   ----main----
    mrMergeOverlappedPost = 5
} MergeResult;

@protocol HLOCDataProvider

- (NSUInteger) getData:(HLOCV **)hlocs Times:(CFAbsoluteTime **) times;

@end

@interface CSSubContainer : NSObject<HLOCDataProvider> {
    NSUInteger packetCapacity;
    NSUInteger packetCount;
    HLOCV *packetHLOCs;
    CFAbsoluteTime *packetTimes;
    
    CFTimeZoneRef defZone;
    

    double maxCurrentCandleInterval;
    double currentCandleInterval;
    
    CandleInterval candleItemInterval;
    
    CFGregorianDate feedingTime;
    int dayOfWeek; // for feeding
}

@property (nonatomic, weak) NSString *lastScaleId;
@property (nonatomic) int lastEffectiveStartIndex;
@property (nonatomic) int lastEffectiveLineLength;
@property (nonatomic) float lastEffectiveClose;
@property (nonatomic) float lastEffectivePercentMax;
@property (nonatomic) float lastEffectivePercentMin;
@property (nonatomic, strong) NSString *decimalledFormat; // CSMultiScale has decimalledFormat string for percent mode, each container has price decimalledFormat.

- (id) initWithPacket:(CSSubContainer *)prev Zone:(CFTimeZoneRef) zone CI:(CandleInterval)theCandleItemInterval DecFormat:(NSString *)decimalledFormat;
- (NSUInteger) getData:(HLOCV **)hlocs Times:(CFAbsoluteTime **) times;
- (void) feedH:(float)h L:(float)l O:(float)o C:(float)c V:(float)v AtTime:(CFAbsoluteTime)t;
- (FeedingResult) appendSkeletonData:(id<HLOCDataProvider>) nextPacket;
- (MergeResult) tryMerge:(CSSubContainer *)sub InsertionCount:(NSUInteger *)insCount; // returns TRUE if fully merged

@end
