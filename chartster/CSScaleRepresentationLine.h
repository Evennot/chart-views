//
//  CSScaleRepresentationLine.h
//  chartster
//
//  Created by Danila Parhomenko on 4/17/13.
//  Copyright (c) 2013 Itheme. All rights reserved.
//

#import "CSAbstractScaleRepresentation.h"

@interface CSScaleRepresentationLine : CSAbstractScaleRepresentation  {
    CGPoint *line;
    CGPoint *animline;
}

@property (nonatomic) CGFloat lastTagWidth; // for long touch tags used in multiscalerepresentation

- (void) addChartSkyLineTo:(CGContextRef) context;

@end


@interface CSScaleRepresentationSubLine : CSScaleRepresentationLine {
    // tag properties
    CGImageRef tagLeft0, tagLeftx, tagLeft1;
    CGImageRef tagRight0, tagRightx, tagRight1;
}

@property (nonatomic) NSUInteger uniqueIdx;
@property (nonatomic) float xoffset;

- (id) initWithScaler:(id<SmoothScalerProtocol>)sc ColorIndex:(NSUInteger) uidx;
- (void) drawLeftTagOn:(CGContextRef) context P:(CGPoint)p Text:(NSString *)price;
- (void) drawRightTagOn:(CGContextRef) context P:(CGPoint)p Text:(NSString *)price;

@end