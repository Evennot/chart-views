//
//  CSScale.h
//  chartster
//
//  Created by Danila Parhomenko on 4/12/13.
//  Copyright (c) 2013 Itheme. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSEssentials.h"
#import "CSAbstractScaleRepresentation.h"
#import "CSAbstractScale.h"
#import "CSSubContainer.h"

@interface CSScale : CSAbstractScale <HLOCDataProvider> {
    HLOCV *hlocs;
    CFAbsoluteTime *times;
    NSUInteger count;
    NSUInteger capacity;
    CGFloat desiredItemSize;
    
    CandleInterval candleItemInterval;
    
    double currentCandleInterval;
    double maxCurrentCandleInterval;
    
    CFGregorianDate feedingTime;
    int dayOfWeek; // for feeding
}

// inherits @property CGFloat desiredItemSize; // desired item size for the next realign routine.

@property (nonatomic, setter = setType:) ScaleRepresentation type;
// inherits @property CSAbstractScaleRepresentation *representation;
// inherits @property CandleInterval candleItemInterval;

- (id) initFor:(CandleInterval) theCandleItemInterval As:(ScaleRepresentation) t;
// inherits - (void) recalcViewInFrame:(CGRect) intFrame ClippingInset:(CGFloat) cInset;
// inherits - (void)pan:(float) x;
// inherits - (float)tryPan:(float) x; // returns panning difficulty: ~1: OK, ~0: difficult, < 0 impossible
// inherits - (double) priceAtY:(float) y;
// inherits - (float) yForPrice:(double) p;
// inherits - (BOOL) drawPresentationInContext:(CGContextRef) context Frame:(CGRect) intFrame Done:(float) drawen;
// inherits - (void) drawInContext:(CGContextRef) context;
- (void) beginFeeding:(NSUInteger) minimumCapacity;
- (FeedingResult) feedingDone;
- (NSString *)getTimeForX:(int) x ItemUsed:(NSUInteger *)itemIndex;
// inherits - (void) enumerateVerticalGridUsingBlock:(GridEnum) block ForMinimalWidth:(float) minW IntFrame:(CGRect) intFrame;
// inherits - (void) dropData;
- (void) feedH:(float)h L:(float)l O:(float)o C:(float)c V:(float)v AtTime:(CFAbsoluteTime)time;
// inherits - (double) startIndexMod:(int) modFactor;
// inherits - (double) lastClose;
// inherits - (CGFloat) positionTensionAround:(CGFloat *)pos;
- (NSUInteger) getData:(HLOCV **)hlocs Times:(CFAbsoluteTime **)time;
- (void) recalcViewInFrame:(CGRect)intFrame ClippingInset:(CGFloat)cInset;

@end

