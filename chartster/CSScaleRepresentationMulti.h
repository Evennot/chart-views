//
//  CSScaleRepresentationMulti.h
//  GICTestApp
//
//  Created by Danila Parhomenko on 6/6/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "CSAbstractScaleRepresentation.h"
#import "CSScaleRepresentationLine.h"
#import "CSSubContainer.h"

@interface CSScaleRepresentationMulti : CSScaleRepresentationLine <SmoothScalerProtocol> {
}

@property (nonatomic, strong, readonly) NSMutableDictionary *subs;
@property (nonatomic, strong) NSArray *subAttributes;

- (id) initWithScaler:(id<SmoothScalerProtocol>)sc;

- (void) setLineLengthForContainers:(NSDictionary *)hlocs MainScale:(NSString *)mainScaleId ItemSize:(CGFloat)curItemSize RepresentationFrame:(CGRect) rFrame VolumeFrame:(CGRect) rVolume;

- (void) drawParameter:(NSString *) param PresentationInContext:(CGContextRef) context Drawen:(float) drawen Addendum:(float) add;
- (void) drawPresentationInContext:(CGContextRef) context Drawen:(float) drawen Addendum:(float) add;
- (void) drawInContext:(CGContextRef)context;
- (void) drawHint:(NSString *) hint On:(CGContextRef) context At:(CGPoint) p Font:(UIFont *)font ScaleKey:(NSString *)mkey;

@end
