//
//  CSView.h
//  chartster
//
//  Created by Itheme on 4/2/13.
//  Copyright (c) 2013 Itheme. All rights reserved.
//
// CSView provides overall chart layout: interval toolbar, axis fonts, and their widths
//
//************************************************************************************************
// Hierarchy:
//
//  /--CSView----------------------------------------------------\
//  |                                                            |
//  |   /--CSGraphicLayer - CALayer of CSView----------------\   |
//  |   |  animation timer, tension support, etc             |   |
//  |   |                                                    |   |
//  |   |   /--CSScale / CSMultiScale -------------------\   |   |
//  |   |   |  for current and newcoming intervals       |   |   |
//  |   |   |  of one or multiple charts correspondingly |   |   |
//  |   |   |                                            |   |   |
//  |   |   |  /--CSScaleRepresentation****----------\   |   |   |
//  |   |   |  |  for candles/bars/lines/multichart  |   |   |   |
//  |   |   |  \-------------------------------------/   |   |   |
//  |   |   |                                            |   |   |
//  |   |   \--------------------------------------------/   |   |
//  |   |                                                    |   |
//  |   \----------------------------------------------------/   |
//  |                                                            |
//  |  intervalToolbar, typeSwitcherButton,                      |
//  |  axis labels,                                              |
//  |  gesture recognizers                                       |
//  \------------------------------------------------------------/
//************************************************************************************************
#import <UIKit/UIKit.h>
#import "CSEssentials.h"

typedef enum ViewRotationTag {
    vrLeft = -1,
    vrStraight = 0,
    vrRight = 1
} ViewRotation;

typedef enum PinchDirectionTag {
    pdSmaller = -1,
    pdUnknown = 0,
#ifdef mywarns
#warning ^^ not used
#endif

    pdBigger = 1
} PinchDirection;

@protocol GICChartViewDelegate

- (void) userPinched:(double) zoom Direction:(PinchDirection) dir;
// zoom is somewhere between 0 (minimum candle size) and ~1 (maximum item size)

@optional
// caption
- (void) captionRectChanged:(CGRect) newRect Color:(UIColor *)mainChartColor;
- (void) captionWillDisappear;
- (void) captionWillAppear:(__weak UIView *)captionView;
// if delegate implements following method, chart will display last price and it's change
// in the cpation (if visible)
- (void) getLast:(out double*)last Change:(out double *)change;

@end

@interface CSView : UIView <UIScrollViewDelegate>

@property (nonatomic) int decimals;

@property (nonatomic, setter = setCandleItemInterval:, getter = getCandleItemInterval) CandleInterval candleItemInterval;
@property (nonatomic, strong) UIFont *labelFont;    // default: size 10 system font
@property (nonatomic) CGFloat toolbarHeight;        // default: 32
@property (nonatomic) CGFloat verticalAxisWidth;    // default: 70
@property (nonatomic) CGFloat horizontalAxisHeight; // default: 20
@property (nonatomic, setter = setHasCaption:, getter = getHasCaption) BOOL hasCaption;              // default: NO - caption above the chart
@property (nonatomic) CGFloat captionHeight;        // default: 24
@property (nonatomic, setter = setPercentMode:, getter = getPercentMode) BOOL percentMode;
@property (nonatomic, readonly) ViewRotation rotation;
@property (nonatomic, setter = setChartType:, getter = getChartType) ScaleRepresentation type; // temporary. Coming soon: bars/candles/line enum
@property (nonatomic, setter = setAnimated:, getter = getAnimated) BOOL animated;

@property (nonatomic, setter = setLineWidth:, getter = getLineWidth) NSNumber *lineWidth;
@property (nonatomic, setter = setLineColor:, getter = getLineColor) UIColor *lineColor;
@property (nonatomic, setter = setVerticalGridColor:, getter = getVerticalGridColor) UIColor *verticalGridColor;
@property (nonatomic, setter = setHorizontalGridColor:, getter = getHorizontalGridColor) UIColor *horizontalGridColor;
@property (nonatomic, setter = setShowVerticalStripes:, getter = getShowVerticalStripes) BOOL showVerticalStripes;
@property (nonatomic, setter = setStripeColor:, getter = getStripeColor) UIColor *stripeColor;
@property (nonatomic, setter = setVerticalGridWidth:, getter = getVerticalGridWidth) NSNumber *verticalGridWidth;
@property (nonatomic, setter = setHorizontalGridWidth:, getter = getHorizontalGridWidth) NSNumber *horizontalGridWidth;
@property (nonatomic, setter = setAxisWidth:, getter = getAxisWidth) NSNumber *axisWidth;
@property (nonatomic, setter = setEmbeddedGesturesEnabled:) BOOL embeddedGesturesEnabled;

@property (nonatomic, weak) id<GICChartViewDelegate> delegate;
@property (nonatomic, readonly) BOOL longTouchOn;

- (void) realignAnimated:(BOOL)animated;

- (void) resetData; // clear mainchart data for current candleItemInterval
- (void) feedData:(NSArray *)rows WithColumns:(NSArray *) columns SubsId:(NSNumber *)nid Main:(BOOL) mainChartData;

- (void) resetSubScaleData:(NSNumber *)subScaleId; //  clear sub chart data for current interval
//- (void) feedSubScale:(NSNumber *)subScaleId Data:(NSArray *)rows WithColumns:(NSArray *) columns;

- (void) feedNews:(id) newsId At:(NSDate *)time; // stub

- (void) candleIntervalChanged;

- (void) rotate:(ViewRotation) r Frame:(CGRect) frame Around:(CGPoint) p;

//- (void) setLineProperties:(NSDictionary *) lineProperies;
//- (void) setAxisLineProperties:(NSDictionary *) axisLineProperties;
//- (void) setGridProperties:(NSDictionary *) gridProperies;

@end


//
//
//  /--------------------------------------------------------------------------------------\
//  | Caption: GICChartViewDelegate.caption*, hasCaption                                   |
//  |                                                                                      |
//  |           /---(OOOOOOOO)---\                                <-- verticalAxisWidth -->|
//  |-----------|----------------|-------------------------------|                         |
//  |   ······  |   ······      ·|····      ······ ___  ······   |- max                    |
//  |   ·_··/\_ |   ······      ·o--v\      __····/   \ ······   |      \                  |
//  |   /·\/·· \|   ······/\____/·····\    /··\··/     \ /\______|- ...  \                 |
//  |  /······  o   ····_/      ······ \  / ···\/·      V ····   |        \                |
//  | / ······   \  ·__/··      ······  \/  ······      ······   |- ...    > x hLineCount  |
//  |/  ······    \_/·····      ······      ······      ······   |        /                |
//  |   ······      ······      ······      ······      ······   |- min _/                 |
//  |------------------------------------------------------------|                         |
//  |   ··.···   #  ······      ······ #    ·····m      ·····.   |                         |
//  | # ··#···   # #······     #·.···· #    ·#···# m m  ·····#   |                         |
//  | # #·#··# m # #·#·.·# . . #·#·#·· # # .·#·#·# # # .·#···# . |                         |
//  |------------------------------------------------------------|                         |
//  | |           |                        /\                    |                         |
//  | |2010-1-20  |2010-1-21       horizontalAxisHeight          |                         |
//  |                                      \/                    |                         |
//  |--------------------------------------------------------------------------------------|
//  |                           (*)                        /\                     |      / |
//  |       M1        M5        M10       H01        toolbarHeight                |  /\ /  |
//  |                                                      \/                     | /  V   |
//  \--------------------------------------------------------------------------------------/
//                                                                                    ^
//  · - vertical stripes                                            chart mode switch button
//  # - volumes
//

