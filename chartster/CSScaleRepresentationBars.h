//
//  CSScaleRepresentationBars.h
//  GICTestApp
//
//  Created by Danila Parhomenko on 6/4/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "CSAbstractScaleRepresentation.h"

@interface CSScaleRepresentationBars : CSAbstractScaleRepresentation {
    CGPoint *placeholder;
    CGPoint *barBodiesRise;
    CGPoint *barBodiesFall;
    CGPoint *barBodiesSame;
    NSUInteger riseCount;
    NSUInteger fallCount;
    NSUInteger sameCount;

    //CGPoint *highsLows;
    float *tmp;
    /*CGRect *animCandleBodiesRise;
    CGRect *animCandleBodiesFall;
    CGPoint *animCandleBodiesSame;
    CGPoint *animHighsLows;*/
}

@end
