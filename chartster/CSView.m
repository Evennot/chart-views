//
//  CSView.m
//  chartster
//
//  Created by Itheme on 4/2/13.
//  Copyright (c) 2013 Itheme. All rights reserved.
//

#import "CSView.h"
#import <QuartzCore/QuartzCore.h>
#import "CSGraphicLayer.h"

@interface UIImage (Extras)
- (UIImage *)imageByScalingProportionallyToScale:(CGFloat) scaleFactor;
@end

@interface CSView () <CSChartLayoutManager> {
    CGRect intFrame;
    CGRect toolbarRect;
    CGRect captionRect;
    float prevPanX;
    float prevScaleX;
    int usedTimeLabelCount;
    CGFloat defDateWidth;
    CFAbsoluteTime decelerationXStartTime, decelerationSStartTime;
    CGPoint decelerationCenterPoint;
    float velocityX, velocityScale, targetTScale, targetTX;
    float decelerationX0, decelerationX, decelerationCScale;
    BOOL badPan, badPinch, badLongTouch;
    float minimumPriceDelta;
    BOOL intervalChangerUIActive;
    float dotReturnY;
    BOOL _hasCaption;
}

@property (nonatomic, strong) UIPanGestureRecognizer *panGR;
@property (nonatomic, strong) UIPinchGestureRecognizer *pinchGR;
@property (nonatomic, strong) UITapGestureRecognizer *tapGR;
@property (nonatomic, strong) UITapGestureRecognizer *doubleTapGR;
@property (nonatomic, strong) UILongPressGestureRecognizer *longTapGR;

@property (nonatomic, strong) CSGraphicLayer *graphicLayer;
@property (nonatomic, strong) NSString *decimalledFormat;

@property (nonatomic, strong) NSArray *priceLabels;
@property (nonatomic, strong) NSArray *timeLabels;
@property (nonatomic, strong) NSArray *dateLabels;
//@property (nonatomic, strong) UIImageView *deltaLocators;
@property (nonatomic, strong) UIButton *deltaControl;
@property (nonatomic, strong) UILabel *deltaDate0;
@property (nonatomic, strong) UILabel *deltaDate1;
//@property (nonatomic, strong) NSMutableArray *notPlacedNews;
@property (nonatomic, strong) NSMutableArray *news;

@property (nonatomic, strong) UIScrollView *intervalChanger;
@property (nonatomic) int intervalsPerChangerScreen;
@property (nonatomic, strong) NSArray *intervalChangerLabels;
@property (nonatomic, strong) UIImageView *dot;
@property (atomic) BOOL dotAnimation;
@property (nonatomic, strong) UITapGestureRecognizer *intervalChangerTapGR;

@property (nonatomic, strong) UIImageView *toolbarBackground;
@property (nonatomic, strong) UIImageView *separator;
@property (nonatomic, strong) UIButton *typeSwitcherButton;
@property (nonatomic, strong) UIView *captionView;

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@end

@implementation CSView

static NSUInteger hLineCount = 7;
static NSUInteger maxVLineCount = 20;
float frictionX = 200;
float frictionScale = 200;

NSString *kPropertySectionGeneral = @"lines";
NSString *kPropertyKeySectionSubData = @"sub";
NSString *kPropertySectionVGrid = @"vgrid";
NSString *kPropertySectionHGrid = @"hgrid";
NSString *kPropertySectionAxes = @"axis";
NSString *kPropertyKeyWidth = @"Width";
NSString *kPropertyKeyElementWidth = @"StickWidth";
NSString *kPropertyKeyVerticalBars = @"VertBars";
NSString *kPropertyKeyColor = @"Color";
NSString *kPropertyKeyStrokeColor = @"STColor";
NSString *kPropertyKeyFillColor = @"FLColor";
NSString *kPropertyKeyRiseColor = @"RiseColor";
NSString *kPropertyKeyFallColor = @"FallColor";
NSString *kPropertyKeyMonochrome = @"Monochrome";

@synthesize animated;

- (void) layoutSubviews {
    [super layoutSubviews];
    [self realignAnimated:YES];
}

- (void) setupGestures {
    self.panGR = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(userPanned:)];
    //[self.panGR setDirection:UISwipeGestureRecognizerDirectionRight | UISwipeGestureRecognizerDirectionLeft];
    self.panGR.minimumNumberOfTouches = 1;
    [self addGestureRecognizer:self.panGR];
    self.pinchGR = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(userPinched:)];
    self.pinchGR.enabled = YES;
    [self addGestureRecognizer:self.pinchGR];
    self.tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTapped:)];
    self.tapGR.numberOfTouchesRequired = 2;
    [self addGestureRecognizer:self.tapGR];
    self.doubleTapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userDoubleTapped:)];
    self.doubleTapGR.numberOfTapsRequired = 2;
    self.doubleTapGR.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:self.doubleTapGR];
    self.longTapGR = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(userTouchedLong:)];
    self.longTapGR.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:self.longTapGR];
    
    self.intervalChangerTapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(intervalChangerTapped:)];
    self.intervalChangerTapGR.numberOfTapsRequired = 1;
    [self.intervalChanger addGestureRecognizer:self.intervalChangerTapGR];
    
    self.embeddedGesturesEnabled = YES;
}

- (void) setupTypeSwitchButton {
    switch (self.graphicLayer.type) {
        case srCandles:
            [self.typeSwitcherButton setImage:[UIImage imageNamed:@"Icon_candles"] forState:UIControlStateNormal];
            //[self.typeSwitcherButton setImage:[UIImage imageNamed:[self retinaName:@"Icon_candles"]] forState:UIControlStateNormal];
            //[self.typeSwitcherButton setImage:[UIImage imageNamed:[self retinaName:@"Icon_candles_pressed"]] forState:UIControlStateHighlighted];
            break;
        case srBars:
            [self.typeSwitcherButton setImage:[UIImage imageNamed:@"Icon_bar"] forState:UIControlStateNormal];
            //[self.typeSwitcherButton setImage:[UIImage imageNamed:[self retinaName:@"Icon_bar"]] forState:UIControlStateNormal];
            //[self.typeSwitcherButton setImage:[UIImage imageNamed:[self retinaName:@"Icon_bar_pressed"]] forState:UIControlStateHighlighted];
            break;            
        default:
            [self.typeSwitcherButton setImage:[UIImage imageNamed:@"Icon_lines"] forState:UIControlStateNormal];
            //[self.typeSwitcherButton setImage:[UIImage imageNamed:[self retinaName:@"Icon_lines"]] forState:UIControlStateNormal];
            //[self.typeSwitcherButton setImage:[UIImage imageNamed:[self retinaName:@"Icon_lines_pressed"]] forState:UIControlStateHighlighted];
            break;
    }
    
}

//- (NSString *) retinaName:(NSString *)f {
//    if (_graphicLayer.contentsScale > 1.0)
//        return [NSString stringWithFormat:@"%@@2x", f];
//    return f;
//}

- (UIImage *) scaledTopCursorImage:(NSString *)name {
    if (_graphicLayer.contentsScale > 1.0)
        return [[[UIImage imageNamed:[NSString stringWithFormat:@"%@@2x", name]] imageByScalingProportionallyToScale:0.5] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 11, 0, 11)];
    return [[UIImage imageNamed:name] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 11, 0, 11)];
}

- (void) firstLayoutSetup:(CGRect)lFrame TypeSwitcherRect:(CGRect) typeSwitcherRect IntSwitcherRect:(CGRect) intervalSwitcherRect {
    _rotation = vrStraight;
    self.graphicLayer = [[CSGraphicLayer alloc] initWithHorizontalLineCount:hLineCount Owner:self];
    animated = self.graphicLayer.animated;
    
    _graphicLayer.frame = lFrame;
    [self.layer addSublayer:self.graphicLayer];
    
    self.toolbarBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cfilter_BG"]];
    self.toolbarBackground.frame = toolbarRect;
    self.toolbarBackground.userInteractionEnabled = YES;
    [self addSubview:self.toolbarBackground];
    self.separator = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cfilter_devider"]];
    self.separator.frame = CGRectMake(typeSwitcherRect.origin.x - 3, 0, 2, toolbarRect.size.height);
    [self.toolbarBackground addSubview:self.separator];
    self.captionView = [[UIView alloc] init];
    self.captionView.frame = captionRect;
    self.captionView.hidden = YES;
//    UIImage *tmpImage = [[[UIImage imageNamed:@"tagLocators@2x"] imageByScalingProportionallyToScale:0.5] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 6, 0, 6)];
//    
//    self.deltaLocators = [[UIImageView alloc] initWithImage:tmpImage];
//    self.deltaLocators.frame = CGRectMake(0, 0, 70, self.captionHeight);
//    self.deltaLocators.hidden = YES;
//    [self addSubview:self.deltaLocators];
    self.deltaControl = [[UIButton alloc] initWithFrame:CGRectMake(0, 1, 50, self.captionHeight - 1)];
    [self.deltaControl setBackgroundImage:[self scaledTopCursorImage:@"top_cursor_red"] forState:UIControlStateSelected];
    [self.deltaControl setBackgroundImage:[self scaledTopCursorImage:@"top_cursor_green"] forState:UIControlStateNormal];
    self.deltaControl.hidden = YES;
    [self.deltaControl.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [self.deltaControl setTitle:@"-" forState:UIControlStateNormal];
    [self addSubview:self.deltaControl];
    self.deltaDate0 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, self.captionHeight)];
    self.deltaDate0.hidden = YES;
    self.deltaDate0.font = [UIFont systemFontOfSize:10];
    self.deltaDate0.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.deltaDate0];
    self.deltaDate1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, self.captionHeight)];
    self.deltaDate1.hidden = YES;
    self.deltaDate1.font = [UIFont systemFontOfSize:10];
    self.deltaDate1.textAlignment = NSTextAlignmentLeft;
    self.deltaDate1.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.deltaDate1];
    
    [self addSubview:self.captionView];
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicator.frame = self.bounds;
    self.activityIndicator.hidden = NO;
    [self addSubview:self.activityIndicator];
    [self.activityIndicator startAnimating];
    
    NSArray *intNames = @[@"Min1", @"Min10", @"H1", @"D1", @"W1", @"Mo1"];

    CGFloat pageSize = intervalSwitcherRect.size.width / self.intervalsPerChangerScreen;
    CGFloat tempX = pageSize * (self.intervalsPerChangerScreen - 1)/2;
    for (NSString *s in intNames) {
        UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(tempX, 12, pageSize, intervalSwitcherRect.size.height - 18)];
        [l setText:s];
        l.backgroundColor = [UIColor clearColor];// self.backgroundColor;
        l.textColor = [UIColor grayColor];
        l.shadowColor = [UIColor blackColor];
        l.shadowOffset = CGSizeMake(0, -1);
        l.textAlignment = NSTextAlignmentCenter;
        if (_intervalChangerLabels)
            self.intervalChangerLabels = [_intervalChangerLabels arrayByAddingObject:l];
        else
            self.intervalChangerLabels = @[l];
        l.tag = self.intervalChangerLabels.count;
        if (l.tag == self.candleItemInterval)
            l.textColor = [UIColor whiteColor];
        tempX += pageSize;
    }
    self.intervalChanger = [[UIScrollView alloc] initWithFrame:intervalSwitcherRect];
    //self.intervalChanger.contentInset = UIEdgeInsetsMake(0, pageSize*2, 0, pageSize*2);
    [self.intervalChanger setContentSize:CGSizeMake(tempX + pageSize*(self.intervalsPerChangerScreen - 1)/2, intervalSwitcherRect.size.height)];
    self.intervalChanger.delegate = self;
    self.dot = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"whiteDot"]];
    self.dot.frame = CGRectMake((intervalSwitcherRect.size.width / 2) - 4, 4, 5, 5);
    [self.toolbarBackground addSubview:self.intervalChanger];
    [self.toolbarBackground addSubview:self.dot];
    for (UILabel *l in self.intervalChangerLabels)
        [self.intervalChanger addSubview:l];
    //self.intervalChanger.pagingEnabled = YES;
    self.intervalChanger.showsHorizontalScrollIndicator = NO;
    
    [self setupGestures];
    
    self.typeSwitcherButton = [[UIButton alloc] initWithFrame:CGRectInset(typeSwitcherRect, -3, -3)];
    //self.typeSwitcherButton.buttonType = UIButtonTypeCustom;
    [self setupTypeSwitchButton];
    [self.typeSwitcherButton setImageEdgeInsets:UIEdgeInsetsMake(2, 2, 2, 2)];
    [self.typeSwitcherButton addTarget:self action:@selector(changeTypePressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.toolbarBackground addSubview:self.typeSwitcherButton];
    
    NSMutableArray *aPriceLabels = [[NSMutableArray alloc] init];
    if (self.labelFont == nil)
        self.labelFont = [UIFont systemFontOfSize:10];
    for (int i = hLineCount; i--; ) {
        UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(lFrame.size.width + lFrame.origin.x, 0, _verticalAxisWidth, lFrame.size.height*PRICEFRAMEK / (hLineCount + 1))];
        l.textAlignment = NSTextAlignmentRight;
        [l setText:@"-"];
        l.backgroundColor = [UIColor clearColor];// self.backgroundColor;
        [l setFont:self.labelFont];
        l.adjustsFontSizeToFitWidth = YES;
        [self addSubview:l];
        [aPriceLabels addObject:l];
    }
    NSMutableArray *aTimeLabels = [[NSMutableArray alloc] init];
    NSMutableArray *aDateLabels = [[NSMutableArray alloc] init];
    for (int i = 0; i <= maxVLineCount*2; i++) {
        UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, lFrame.origin.y + lFrame.size.height, 16, _horizontalAxisHeight / 2)];
        l.textAlignment = NSTextAlignmentLeft;//NSTextAlignmentCenter;
        [l setText:@"|"];
        l.backgroundColor = [UIColor clearColor];// self.backgroundColor;
        [l setFont:self.labelFont];
        //l.lineBreakMode = NSLineBreakByWordWrapping;
        //l.numberOfLines = 2;
        l.adjustsFontSizeToFitWidth = YES;
        [self addSubview:l];
        if (i < maxVLineCount)
            [aTimeLabels addObject:l];
        else
            [aDateLabels addObject:l];
    }
    self.priceLabels = aPriceLabels;
    self.timeLabels = aTimeLabels;
    self.dateLabels = aDateLabels;

}

- (void) rectAdjust:(CGRect)lFrame TypeSwitcherRect:(CGRect) typeSwitcherRect IntSwitcherRect:(CGRect) intervalSwitcherRect {
    self.toolbarBackground.frame = toolbarRect;
    self.captionView.frame = captionRect;
    self.separator.frame = CGRectMake(typeSwitcherRect.origin.x - 3, 0, 2, toolbarRect.size.height);
    self.dot.frame = CGRectMake((intervalSwitcherRect.size.width / 2) - 4, 4, 5, 5);
    self.intervalChanger.frame = intervalSwitcherRect;
    CGFloat pageSize = intervalSwitcherRect.size.width / self.intervalsPerChangerScreen;
    CGFloat tempX = pageSize * (self.intervalsPerChangerScreen - 1)/2;
    for (UILabel *l in self.intervalChangerLabels) {
        l.frame = CGRectMake(tempX, 12, pageSize, intervalSwitcherRect.size.height - 18);
        tempX += pageSize;
    }
    [self.intervalChanger setContentSize:CGSizeMake(tempX + pageSize*(self.intervalsPerChangerScreen - 1)/2, intervalSwitcherRect.size.height)];

    self.typeSwitcherButton.frame = CGRectInset(typeSwitcherRect, -3, -3);
    _graphicLayer.frame = lFrame;
    for (UILabel *l in self.priceLabels)
        l.frame = CGRectMake(lFrame.size.width + lFrame.origin.x, 0, _verticalAxisWidth, lFrame.size.height*PRICEFRAMEK / (hLineCount + 1));
    for (UILabel *l in self.timeLabels)
        l.frame = CGRectMake(0, lFrame.origin.y + lFrame.size.height, 16, _horizontalAxisHeight / 2);
    for (UILabel *l in self.dateLabels)
        l.frame = CGRectMake(0, lFrame.origin.y + lFrame.size.height, 16, _horizontalAxisHeight / 2);
}

#ifdef mywarns
#warning MORE FRICTION!
#endif

- (void) realignAnimated:(BOOL)xanimated {
    double xrescale = intFrame.size.width;
    
    intFrame = CGRectInset([self bounds], 2, 0);
    
    if (xanimated) {
        if (xrescale > 1.0)
            xrescale = intFrame.size.width / xrescale;
        else {
            xrescale = 1.0;
            xanimated = NO;
        }
    } else
        xrescale = 1.0;
    
    if (_verticalAxisWidth < 1.0)
        _verticalAxisWidth = 50;
    if (_horizontalAxisHeight < 1.0)
        _horizontalAxisHeight = 20;
    if (_toolbarHeight < 5.0)
        _toolbarHeight = 36;
    if (_captionHeight < 5.0)
        _captionHeight = 24;
    //_toolbarIsOnTop = YES;
    //_hasCaption = YES;
    CGRect lFrame = CGRectOffset(CGRectInset(intFrame, _verticalAxisWidth / 2, (_horizontalAxisHeight + _toolbarHeight) / 2), -_verticalAxisWidth / 2, -_horizontalAxisHeight / 2);
    if (_hasCaption) {
        captionRect = CGRectMake(0, 2, lFrame.size.width, _captionHeight - 4);
        lFrame.origin.y += _captionHeight;
        lFrame.size.height -= _captionHeight;
    }
    lFrame.origin.y -= _toolbarHeight / 2;
    
    toolbarRect = CGRectMake(0, (intFrame.size.height - _toolbarHeight), intFrame.size.width + 4, _toolbarHeight);
    CGFloat h = toolbarRect.size.height - 4;
    CGRect intervalSwitcherRect = CGRectMake(2, 2, toolbarRect.size.width - 8 - h, h);
    CGRect typeSwitcherRect = CGRectMake(toolbarRect.size.width - h - 2, 2, h, h);
    usedTimeLabelCount = lFrame.size.width / 30;
    defDateWidth = lFrame.size.width * 2 / usedTimeLabelCount;
    #ifdef mywarns
    #warning In case of variable interval count modify this \/
    #endif
    if (intervalSwitcherRect.size.width / 3 < 32) {
        self.intervalsPerChangerScreen = 1;
    } else {
        if (intervalSwitcherRect.size.width / 5 < 40)
            self.intervalsPerChangerScreen = 3;
        else
            self.intervalsPerChangerScreen = (intervalSwitcherRect.size.width / 7 < 60)?5:7;
    }

    if (self.graphicLayer == nil) {
        [self firstLayoutSetup:lFrame TypeSwitcherRect:typeSwitcherRect IntSwitcherRect:intervalSwitcherRect];
        if (_hasCaption && [((NSObject*)_delegate) respondsToSelector:@selector(captionWillAppear:)]) {
            [_delegate captionWillAppear:self.captionView];
        }
    } else
        [self rectAdjust:lFrame TypeSwitcherRect:typeSwitcherRect IntSwitcherRect:intervalSwitcherRect];
    if (_hasCaption) {
        self.captionView.hidden = NO;
        if ([((NSObject*)_delegate) respondsToSelector:@selector(captionRectChanged:Color:)]) {
            NSDictionary *gen = [self.graphicLayer.visualProps valueForKey:kPropertySectionGeneral];
            [_delegate captionRectChanged:captionRect Color:[gen valueForKey:kPropertyKeyColor]];
            //if ([((NSObject*)_delegate) respondsToSelector:@selector(getLast:Change:)]) {
                //double last, change;
                //[_delegate getLast:&last Change:&change];
                //[self.graphicLayer setLast:last];
                //NSString *x = [NSString stringWithFormat:self.decimalledFormat, change];
                //if (change > EPSILON)
                //    x = [NSString stringWithFormat:@"+%@", x];
                //else
                //    if (change > -EPSILON)
                //        x = @"";
                //x = [NSString stringWithFormat:@"%@ %@", [NSString stringWithFormat:self.decimalledFormat, last], x];
            //}
        }
    } else {
        if (!self.captionView.hidden) {
            if ([((NSObject*)_delegate) respondsToSelector:@selector(captionWillDisappear)])
                [_delegate captionWillDisappear];
            self.captionView.hidden = NO;
        }
    }

    [self.intervalChanger scrollRectToVisible:CGRectMake((self.candleItemInterval - 1)*intervalSwitcherRect.size.width/self.intervalsPerChangerScreen, 0, intervalSwitcherRect.size.width, intervalSwitcherRect.size.height) animated:YES];

    [self.priceLabels enumerateObjectsUsingBlock:^(UILabel *l, NSUInteger idx, BOOL *stop) {
        CGRect f = l.frame;
        f.origin.x = intFrame.origin.x + lFrame.size.width + lFrame.origin.x;
        f.origin.y = f.size.height * (idx + 0.5);
        l.frame = f;
    }];
    CGFloat y0 = intFrame.origin.y + intFrame.size.height - _horizontalAxisHeight - _toolbarHeight;
    [self.timeLabels enumerateObjectsUsingBlock:^(UILabel *l, NSUInteger idx, BOOL *stop) {
        CGRect f = l.frame;
        if (idx < usedTimeLabelCount) {
            f.size.width = lFrame.size.width / usedTimeLabelCount;
            f.origin.x = intFrame.origin.x + idx * lFrame.size.width / usedTimeLabelCount;
            f.origin.y = y0;
            l.frame = f;
            //l.hidden = NO;
        }// else
        l.hidden = YES;
    }];
    y0 += _horizontalAxisHeight / 2;
    [self.dateLabels enumerateObjectsUsingBlock:^(UILabel *l, NSUInteger idx, BOOL *stop) {
        CGRect f = l.frame;
        if (idx < usedTimeLabelCount) {
            f.size.width = defDateWidth;
            f.origin.x = intFrame.origin.x + idx * lFrame.size.width / usedTimeLabelCount;
            f.origin.y = y0;
            l.frame = f;
            //l.hidden = NO;
        }// else
        l.hidden = YES;
    }];

    [_graphicLayer realignAnimated:xanimated XRescale:xrescale];
    self.activityIndicator.frame = self.bounds;

}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
#ifdef mywarns
#warning display loading indicator here... maybe
#endif
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset  {
    CGFloat w = scrollView.frame.size.width/self.intervalsPerChangerScreen;
    CGFloat rest = fmod((*targetContentOffset).x, w);
    if (rest > 0.5*w) {
        (*targetContentOffset).x += -rest + w;
    } else
        (*targetContentOffset).x += -rest;
    intervalChangerUIActive = YES;
    self.candleItemInterval = ((*targetContentOffset).x / w) + 1.01; // in case of improper fmod
    intervalChangerUIActive = NO;
}

- (void) intervalChangerTapped:(UITapGestureRecognizer *) tapGR {
    if (tapGR.state != UIGestureRecognizerStateRecognized) return;
    CGPoint p = [tapGR locationOfTouch:0 inView:self.intervalChanger];
    for (UILabel *l in self.intervalChangerLabels)
        if (CGRectContainsPoint(l.frame, p)) {
            self.candleItemInterval = l.tag;
            return;
        }
}

- (void) changeTypePressed:(UIButton *) button {
    if (self.graphicLayer.type == srBars)
        self.type = srLine;
    else
        self.type = self.type + 1;
}

- (void) setVerticalAxisWidth:(CGFloat)w {
    if (ABS(w - _verticalAxisWidth) < 0.1) return;
    _verticalAxisWidth = w;
    [self realignAnimated:NO];
}

- (void) setHorizontalAxisHeight:(CGFloat)h {
    if (ABS(h - _horizontalAxisHeight) < 0.1) return;
    _horizontalAxisHeight = h;
    [self realignAnimated:NO];
}

- (void) setEmbeddedGesturesEnabled:(BOOL)e {
    self.pinchGR.enabled = self.tapGR.enabled = self.panGR.enabled = _embeddedGesturesEnabled = e;
}

- (BOOL) gestureIsOutOfLayer:(UIGestureRecognizer *)gesture OneTouch:(BOOL) one {
    if (gesture.numberOfTouches > 0) {
        if ([gesture locationOfTouch:0 inView:self.toolbarBackground].y > 0)
            return YES;
        if (!one && (gesture.numberOfTouches > 1))
            return ([gesture locationOfTouch:1 inView:self.toolbarBackground].y > 0);
    }
    return NO;
}

- (void) userPanned:(UIPanGestureRecognizer *)gesture {
    CGPoint p = [gesture translationInView:self];
    if (gesture.state == UIGestureRecognizerStateBegan) {
        prevPanX = p.x;
        _graphicLayer.decelerating = dsNotYet;
        badPan = [self gestureIsOutOfLayer:gesture OneTouch:YES];
        return;
    }
    if ((gesture.state == UIGestureRecognizerStateRecognized) || (gesture.state == UIGestureRecognizerStateChanged)) {
        if (gesture.numberOfTouches > 0) {
            if ([gesture locationOfTouch:0 inView:self.toolbarBackground].y > 0)
                return;
            badPan = NO;
        } else
            if (badPan)
                return;
        [self.graphicLayer pan:prevPanX - p.x];
        prevPanX = p.x;
        [self.graphicLayer recalcViewInFrame:self.graphicLayer.frame];
        if (gesture.state == UIGestureRecognizerStateRecognized) {
            p = [gesture velocityInView:self];
            _graphicLayer.decelerating = dsPanAfterTouch;
            if (ABS(p.x) > 10) {
                velocityX = p.x/2;
                decelerationX = decelerationX0 = 0;
                decelerationXStartTime = CFAbsoluteTimeGetCurrent();
            } else
                _graphicLayer.decelerating = dsStill; // resetting dsAfterTouch to check tension
        } else {
            _graphicLayer.decelerating = dsNotYet;
        }
        [self presentationEnded];
    }
}

- (void) userPinched:(UIPinchGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateBegan) {
        prevScaleX = gesture.scale;
        _graphicLayer.decelerating = dsNotYet;
        badPinch = [self gestureIsOutOfLayer:gesture OneTouch:NO];
        return;
    }
    if ((gesture.state == UIGestureRecognizerStateRecognized) || (gesture.state == UIGestureRecognizerStateChanged)) {
        if (gesture.numberOfTouches > 1) {
            if (([gesture locationOfTouch:0 inView:self.toolbarBackground].y > 0) || ([gesture locationOfTouch:1 inView:self.toolbarBackground].y > 0))
                return;
            badPinch = NO;
        } else
            if (badPinch)
                return;
        if (prevScaleX > 0.00001) {
            CGPoint p = [gesture locationInView:self];
            p.x -= intFrame.origin.x;
            p.y -= intFrame.origin.y;
            [self.graphicLayer rescale:gesture.scale/prevScaleX Around:p];
            //itemSize = startItemSize * gesture.scale;
            [self.graphicLayer recalcViewInFrame:self.graphicLayer.frame];
            if (gesture.state == UIGestureRecognizerStateRecognized) {
                velocityScale = [gesture velocity];
                if (ABS(velocityScale) > 0.1) {
                    decelerationCScale = gesture.scale;
                    decelerationCenterPoint = p;
                    decelerationSStartTime = CFAbsoluteTimeGetCurrent();
                    _graphicLayer.decelerating = dsScaleAfterTouch;
                    _graphicLayer.decelerating = dsStill;
                }
            }
            [self presentationEnded];
            if (self.delegate) {
                CGFloat x = (_graphicLayer.desiredItemSize - 1.5) * 3 / _graphicLayer.frame.size.width;
                if (prevScaleX > gesture.scale)
                    [self.delegate userPinched:x Direction:pdSmaller];
                else
                    [self.delegate userPinched:x Direction:pdBigger];
            }
            prevScaleX = gesture.scale;
        }
    }
}

//- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    if (self.trackedTouches == nil)
//        self.trackedTouches = [[NSMutableArray alloc] init];
//    for (UITouch *t in touches)
//        [self.trackedTouches addObject:t];
//    NSLog(@"Started %d", self.trackedTouches.count);
//
//}
//
//- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
//    NSLog(@"Moved");
//}
//
//- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
//    NSLog(@"Ended");
//}
//
//- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
//    NSLog(@"Canceled");
//}

- (void) userTouchedLong:(UILongPressGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateBegan) {
        //NSLog(@"Long. Began");
        if ([self gestureIsOutOfLayer:gesture OneTouch:YES]) {
            badLongTouch = YES;
            _longTouchOn = NO;
            return;
        }
        badLongTouch = NO;
        _longTouchOn = YES;
        CGFloat x = [gesture locationOfTouch:0 inView:self].x - self.graphicLayer.frame.origin.x;
        [self.graphicLayer hintOn:x Block:^(NSArray *data, NSString *date) {
            [self.deltaDate0 setText:data[0]];
            [self.deltaDate1 setText:date];
        }];
    } else {
        if (badLongTouch) return;
        if ((gesture.state == UIGestureRecognizerStateChanged) || (gesture.state == UIGestureRecognizerStateRecognized)) {
            _longTouchOn = YES;
            CGFloat x = [gesture locationOfTouch:0 inView:self].x - self.graphicLayer.frame.origin.x;
            [self.graphicLayer hintOn:x Block:^(NSArray *data, NSString *date) {
                [self.deltaDate0 setText:data[0]];
                [self.deltaDate1 setText:date];
                self.deltaDate0.textAlignment = NSTextAlignmentLeft;
            }];
        } else {
            _longTouchOn = NO;
            [self.graphicLayer unmark];
        }
    }
}

- (void) userTapped:(UITapGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateRecognized) {
        if ([self gestureIsOutOfLayer:gesture OneTouch:NO]) return;
        CGFloat x0 = [gesture locationOfTouch:0 inView:self].x - self.graphicLayer.frame.origin.x;
        CGFloat x1 = [gesture locationOfTouch:1 inView:self].x - self.graphicLayer.frame.origin.x;
        [self.graphicLayer markZoneBetween:x0 And:x1 Block:^(double delta, NSString *date0, NSString *date1) {
            NSString *t = nil;
            if (delta > EPSILON) {
                self.deltaControl.selected = NO;
                t = [NSString stringWithFormat:@"+%@", [self priceFormattedForAxis:delta]];
            } else {
                if (delta > -EPSILON)
                    t = @"No change";
                else {
                    self.deltaControl.selected = YES;
                    t = [self priceFormattedForAxis:delta];
                }
            }
            [self.deltaControl setTitle:t forState:UIControlStateNormal];
            [self.deltaControl setTitle:t forState:UIControlStateSelected];
            [self.deltaDate0 setText:date0];
            self.deltaDate0.textAlignment = NSTextAlignmentRight;
            [self.deltaDate1 setText:date1];
        }];
    }
}

- (void) zoneMarkMovedToP1:(CGPoint)p1 P2:(CGPoint)p2 {
    if (!_hasCaption) return;
    if (CGPointEqualToPoint(p1, CGPointZero)) {
        if (self.deltaControl.hidden) return;
        if ([((NSObject*)_delegate) respondsToSelector:@selector(captionWillAppear:)]) {
            [_delegate captionWillAppear:self.captionView];
        }
        [UIView animateWithDuration:0.5 animations:^{
            //self.deltaLocators.alpha = 0.0;
            self.deltaControl.alpha = 0.0;
            self.deltaDate0.alpha = 0.0;
            self.deltaDate1.alpha = 0.0;
            self.captionView.alpha = 1.0;
            self.deltaDate0.alpha = 0.0;
        } completion:^(BOOL finished) {
            self.deltaControl.hidden = YES;
            self.deltaControl.alpha = 1.0;
            self.deltaDate0.hidden = YES;
            self.deltaDate0.alpha = 1.0;
            self.deltaDate1.hidden = YES;
            self.deltaDate1.alpha = 1.0;
            //self.deltaLocators.hidden = YES;
            //self.deltaLocators.alpha = 1.0;
        }];
    } else {
        if (self.deltaControl.hidden)
            if ([((NSObject*)_delegate) respondsToSelector:@selector(captionWillDisappear)]) {
                [_delegate captionWillDisappear];
            }
        CGFloat locatorsWidth = p2.x - p1.x;
        if (locatorsWidth < 50) {
            p1.x -= (50 - locatorsWidth)/2;
            locatorsWidth = 50;
        }
            
        self.deltaControl.frame = CGRectMake(p1.x, 2, locatorsWidth, self.captionHeight - 4);
        self.deltaDate0.frame = CGRectMake(p1.x - 64, 0, 60, self.captionHeight);
        self.deltaDate1.frame = CGRectMake(p1.x + locatorsWidth + 4, 0, 60, self.captionHeight);
            //self.deltaLocators.frame = CGRectMake(p1.x, 0, locatorsWidth, self.captionHeight);
            if (self.deltaControl.hidden) {
                self.deltaControl.alpha = 0.0;
                self.deltaControl.hidden = NO;
                self.deltaDate0.alpha = 0.0;
                self.deltaDate0.hidden = NO;
                self.deltaDate1.alpha = 0.0;
                self.deltaDate1.hidden = NO;
                //self.deltaLocators.hidden = NO;
                [UIView animateWithDuration:0.5 animations:^{
                    self.deltaControl.alpha = 1.0;
                    self.captionView.alpha = 0.0;
                    self.deltaDate0.alpha = 1.0;
                    self.deltaDate1.alpha = 1.0;
                }];
            }
    }
}

- (void) singleMarkMovedToP1:(CGPoint)p {
    if (!_hasCaption) return;
    if (CGPointEqualToPoint(p, CGPointZero)) {
        if (self.deltaDate0.hidden) return;
        if ([((NSObject*)_delegate) respondsToSelector:@selector(captionWillAppear:)]) {
            [_delegate captionWillAppear:self.captionView];
        }
        [UIView animateWithDuration:0.5 animations:^{
            self.captionView.alpha = 1.0;
            self.deltaDate0.alpha = 0.0;
            self.deltaDate1.alpha = 0.0;
        } completion:^(BOOL finished) {
            self.deltaDate0.hidden = YES;
            self.deltaDate0.alpha = 1.0;
            self.deltaDate1.hidden = YES;
            self.deltaDate1.alpha = 1.0;
        }];

    } else {
        if (self.deltaDate0.hidden)
            if ([((NSObject*)_delegate) respondsToSelector:@selector(captionWillDisappear)]) {
                [_delegate captionWillDisappear];
            }
        self.deltaDate0.frame = CGRectMake(p.x, 0, 80, self.captionHeight / 2);
        self.deltaDate1.frame = CGRectMake(p.x, self.captionHeight / 2, 80, self.captionHeight / 2);
        if (self.deltaDate0.hidden) {
            self.deltaDate0.alpha = 0.0;
            self.deltaDate0.hidden = NO;
            self.deltaDate1.alpha = 0.0;
            self.deltaDate1.hidden = NO;
            [UIView animateWithDuration:0.5 animations:^{
                self.captionView.alpha = 0.0;
                self.deltaDate0.alpha = 1.0;
                self.deltaDate1.alpha = 1.0;
            }];
        }
    }

}

- (void)setHasCaption:(BOOL) v {
    if (v == _hasCaption) return;
    _hasCaption = v;
    self.captionView.hidden = !v;
    [self realignAnimated:YES];
}

- (BOOL)getHasCaption {
    return _hasCaption;
}

- (void) userDoubleTapped:(UITapGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateRecognized) {
        if ([self gestureIsOutOfLayer:gesture OneTouch:NO]) return;
        [self.graphicLayer unmark];
        self.graphicLayer.decelerating = dsScaleAfterTouch; // pseudo touch by realigning
        [self realignAnimated:YES];
        [self presentationEnded];
        self.graphicLayer.decelerating = dsStill; // pseudo touch ended
        //self.graphicLayer.deceleratingVScale = dsNone; // reset vertical scale
        [self.graphicLayer setNeedsDisplay];
    }
}


- (void) presentationStarted {
    //for (UILabel *l in self.priceLabels)
    //    [l setText:@"-"];
}

- (void) decelerate {
    CFAbsoluteTime at = CFAbsoluteTimeGetCurrent();
    CFTimeInterval dt;
    if (_graphicLayer.decelerating & dsAfterTouches) {
        dt = at - decelerationXStartTime;
        float x;
//        if (dt*frictionX*2 >= ABS(velocityX)) {
//            dt = ABS(velocityX) / frictionX / 2;
//            _graphicLayer.deceleratingX = dsNone;
//        }
        float v;
        if (velocityX > 0) {
            if (velocityX > frictionX*0.5)
                velocityX = frictionX;
            v = velocityX - frictionX*dt;
        } else {
            if (velocityX < -frictionX*0.5)
                velocityX = -frictionX;
            v = velocityX + frictionX*dt;
        }
        if (velocityX * v < 0) {
            _graphicLayer.decelerating = dsStill;
        } else {
            //x = v * dt;
            //NSLog(@"velocityX = %f v = %f", velocityX, v);
            if (velocityX > 0)
                x = decelerationX0 + velocityX*dt - (frictionX*dt*dt/2);
            else
                x = decelerationX0 + velocityX*dt + (frictionX*dt*dt/2);
            float panRes = [self.graphicLayer tryPan:decelerationX - x];
            if (panRes < 0)
                _graphicLayer.decelerating = dsStill;
            else {
                decelerationX = x;
                if (panRes < 0.99) {
                    decelerationX0 = x;
                    velocityX = v * 0.5;//panRes;
                    decelerationXStartTime = at;
                }
            }
        }
        
    }
    if (_graphicLayer.decelerating == dsScaleAfterTouch) {
        dt = at - decelerationSStartTime;
        float x;
        if (dt > 0.5) {
            x = targetTScale;
            _graphicLayer.decelerating = dsStill;
        } else
            x = ((targetTScale * dt) + (0.5 - dt))/0.5;
        float comp = x/decelerationCScale;
        if (ABS(comp - 1.0) < EPSILON) {
            x = targetTScale;
            _graphicLayer.decelerating = dsStill;
        }
#warning extract a method!
        [self.graphicLayer rescale:comp Around:decelerationCenterPoint];
        decelerationCScale = x;
    }
    [self.graphicLayer recalcViewInFrame:self.graphicLayer.frame];
    [self presentationEnded];
}

- (void) compensateTension {
    CFAbsoluteTime at = CFAbsoluteTimeGetCurrent();
    CFTimeInterval dt;
    float x;
    if (_graphicLayer.decelerating == dsPanTensionCompensation) {
        dt = at - decelerationXStartTime;
        if (dt > 0.5) {
            x = targetTX;
            _graphicLayer.decelerating = dsStill;
        } else
            x = (targetTX * dt)/0.5;
            
        if ([self.graphicLayer tryPan:x - decelerationX] > 0)
            decelerationX = x;
        else
            _graphicLayer.decelerating = dsStill;
    }
    if (_graphicLayer.decelerating == dsScaleTensionCompensation) {
        dt = at - decelerationSStartTime;
        if (dt > 0.5) {
            x = targetTScale;
            _graphicLayer.decelerating = dsStill;
        } else
            x = ((targetTScale * dt) + (0.5 - dt))/0.5;
        float comp = x/decelerationCScale;
        if (ABS(comp - 1.0) < 0.001) {
            x = targetTScale;
            _graphicLayer.decelerating = dsStill;
        }
        [self.graphicLayer rescale:comp Around:decelerationCenterPoint];
        decelerationCScale = x;
    }
    [self.graphicLayer recalcViewInFrame:self.graphicLayer.frame];
    [self presentationEnded];
}

- (void) setPercentMode:(BOOL)pm {
    if (pm == self.graphicLayer.percentMode)
        return;
    self.graphicLayer.percentMode = pm;
}

- (BOOL) getPercentMode {
    return self.graphicLayer.percentMode;
}

- (NSString *) priceFormattedForAxis:(double) p {
    if (p < -EPSILON)
        return [NSString stringWithFormat:@"-%@", [self priceFormattedForAxis:-p]];
    NSString *v;
    int millions = p / 1000000.0;
    int mill2 = (p + minimumPriceDelta) / 1000000.0;
    if (mill2 > millions) { // Here may come the 9999.999 bug
        p += minimumPriceDelta;
        millions = mill2;
    }
    //if (ABS(m - millions) > 10) // looks like int overflow
    //    return @"";
    if (millions) {
        if (millions < 0)
            p = millions * 1000000.0 - p;
        else
            p-= millions * 1000000.0;
        int t = p / 1000;
        int units = p - (t * 1000);
        return [NSString stringWithFormat:@"%d %.3d %.3d", millions, t, units];
    }
    int t = p / 1000;
    int t2 = (p + minimumPriceDelta) / 1000;
    if (t2 > t) {
        p += minimumPriceDelta;
        t = t2;
    }
    if (t) {
        if (t < 0)
            p = (t * 1000) - p;
        else
            p-= t * 1000;
        v = [NSString stringWithFormat:_decimalledFormat, p];
        int units = p;
        int units2 = p + minimumPriceDelta;
        if (units2 > units)
            units = units2;
        if (units >= 100)
            v = [NSString stringWithFormat:@"%d %@", t, v];
        else
            v = [NSString stringWithFormat:(units >= 10)?@"%d 0%@":@"%d 00%@", t, v];
        return v;
    }
    return [NSString stringWithFormat:_decimalledFormat, p];
}

- (void) presentationEnded { // some action (or part of an action) ended. Actions like presentation, moving, scaling, decelerating... method should be renamed probably
    __block int activeHLines = [self.graphicLayer alignHLines:self.decimals];
    minimumPriceDelta = 0.005;
    if (self.percentMode)
        self.decimalledFormat = @"%.2f %%";
    else {
        self.decimalledFormat = [NSString stringWithFormat:@"%%.%df", self.decimals < 0?0:self.decimals];
        if (self.decimals >= 0)
            minimumPriceDelta = powf(0.1, self.decimals)/2;
    }
    NSString *longestPriceText;
    if ([self getPercentMode])
        longestPriceText = [self priceFormattedForAxis:[_graphicLayer percentPriceAtHLine:0]];
    else
        longestPriceText = [self priceFormattedForAxis:[_graphicLayer priceAtHLine:0]];
    
    CGFloat labelMaxSize = [longestPriceText sizeWithFont:((UILabel *)self.priceLabels[0]).font].width;
    [self.priceLabels enumerateObjectsUsingBlock:^(UILabel *l, NSUInteger idx, BOOL *stop) {
        l.hidden = idx >= activeHLines;
        if (!l.hidden) {
            double p;
            if ([self getPercentMode]) {
                p = [self.graphicLayer percentPriceAtHLine:idx];
            } else
                p = [self.graphicLayer priceAtHLine:idx];
            l.center = CGPointMake(l.center.x, [self.graphicLayer hLineY:idx]);
            CGRect b = l.bounds;
            l.bounds = CGRectMake(b.origin.x, b.origin.y, labelMaxSize, b.size.height);
            [l setText:[self priceFormattedForAxis:p]];
        }
    }];
    __block int timeIdx = 0;
    __block int dateIdx = 0;
#ifdef mywarns
#warning enumerate them on presentation start
#endif
    [self.graphicLayer enumerateVerticalGridUsingBlock:^(NSUInteger idx, NSString *title, NSString *subTitle, float desiredCoordinate) {
        if (title) {
            if (timeIdx < usedTimeLabelCount) {
                UILabel *l = nil;
                if (timeIdx) {
                    l = self.timeLabels[timeIdx - 1];
                    if (desiredCoordinate - l.frame.origin.x > 32)
                        l = nil;
                }
                if (l == nil) {
                    l = self.timeLabels[timeIdx++];
                    CGRect f = l.frame;
                    f.origin.x = desiredCoordinate + 2;
                    //if (l.hidden || ![l.text isEqualToString:title]) {
                        l.frame = f;
                        l.hidden = NO;
                        [l setText:title];
//                    } else
//                        [UIView animateWithDuration:0.5 animations:^{
//                            l.frame = f;
//                        }];
                }
            }
        }
        if (subTitle)
            if (dateIdx < usedTimeLabelCount) {
                UILabel *l = self.dateLabels[dateIdx];
                CGRect f = l.frame;
                f.origin.x = desiredCoordinate + 2;
                f.size.width = defDateWidth;
                l.frame = f;
                l.hidden = NO;
                [l setText:subTitle];
                if (dateIdx == 1) {
                    UILabel *pl = self.dateLabels[0];
                    CGRect plFrame = pl.frame;
                    if (plFrame.origin.x + plFrame.size.width > desiredCoordinate) {
                        plFrame.size.width = desiredCoordinate - plFrame.origin.x;
                        pl.frame = plFrame;
                    }
                }
                dateIdx++;
            }
        
    } ForMinimalWidth:32];
    for (NSUInteger i = self.timeLabels.count; i-- > timeIdx; )
        ((UILabel *)self.timeLabels[i]).hidden = YES;
    for (NSUInteger i = self.dateLabels.count; i-- > dateIdx; )
        ((UILabel *)self.dateLabels[i]).hidden = YES;
    [self.graphicLayer.currentScale checkNeededCompensation:NO ForMasterView:self];
}

- (void) needsScaleTensionCompensationTo:(CGFloat)targetScale {
    if (_graphicLayer.decelerating == dsNotYet) return;
    decelerationCScale = 1;
    //decelerationCenterPoint = self.center;
    targetTScale = targetScale;
    decelerationSStartTime = CFAbsoluteTimeGetCurrent();
    _graphicLayer.decelerating = dsScaleTensionCompensation;
}

- (void) needsPanTensionCompensationToPosition:(CGFloat)pos {
    if (_graphicLayer.decelerating == dsNotYet) return;
    decelerationX = decelerationX0 = 0;
    targetTX = pos;
    decelerationXStartTime = CFAbsoluteTimeGetCurrent();
    _graphicLayer.decelerating = dsPanTensionCompensation;
}

- (CandleInterval) getCandleItemInterval {
    return _graphicLayer.candleItemInterval;
}

- (void) continueDotAnimation {
    dotReturnY = self.dot.center.y;
    [UIView animateWithDuration:0.5 animations:^{
        self.dot.center = CGPointMake(self.dot.center.x, -8);
    }];
}

- (void) candleIntervalChanged {
    [self.intervalChangerLabels enumerateObjectsUsingBlock:^(UILabel *l, NSUInteger idx, BOOL *stop) {
        if (self.candleItemInterval - 1 == idx)
            l.textColor = [UIColor whiteColor];
        else
            l.textColor = [UIColor grayColor];
    }];
    //self.dot.image = [UIImage imageNamed:@"whiteLoadingDot"];
    self.dotAnimation = YES;
    self.activityIndicator.hidden = NO;
    [self.activityIndicator startAnimating];
    [self continueDotAnimation];
    if (intervalChangerUIActive) return;
    CGSize s = self.intervalChanger.frame.size;
    [self.intervalChanger scrollRectToVisible:CGRectMake((self.candleItemInterval - 1)*s.width/self.intervalsPerChangerScreen, 0, s.width, s.height) animated:YES];
    //[_graphicLayer dropData];
    //[_graphicLayer present]; current data will be cleared only when new will come
}

- (void) newScaleLoaded:(CandleInterval) interval {
    self.dotAnimation = NO;
    [UIView animateWithDuration:0.8 animations:^{
        self.dot.center = CGPointMake(self.dot.center.x, dotReturnY);
    }];
    [self.activityIndicator stopAnimating];
    self.activityIndicator.hidden = YES;
}

- (void) setCandleItemInterval:(CandleInterval)value {
    if (value == _graphicLayer.candleItemInterval) return;
    _graphicLayer.candleItemInterval = value;
    [self candleIntervalChanged];
}

- (void) changeCandleItemInterval:(CandleInterval)cInt ByPinch:(PinchDirection) direction {
    #ifdef mywarns
    #warning direction not used? and the whole method??
    #endif
    CandleInterval curInt = _graphicLayer.candleItemInterval;
    if (cInt == curInt) return;
    CGFloat dis = _graphicLayer.desiredItemSize;
    _graphicLayer.candleItemInterval = cInt;
    while (curInt < cInt) {
        if (curInt > ciHour)
            dis *= (curInt == ciWeek)?(30.5/7.0)/*weeks to months*/:(7.0)/*days to weeks*/;
        else {
            if (curInt == ciHour)
                dis *= 24/*hours to days*/;
            else
                dis *= (curInt == ci10Minutes)?6.0:10.0;
        }
        curInt++;
    }
    while (curInt > cInt) {
        if (curInt > ciDay)
            dis *= (curInt == ciWeek)?(1.0/7.0)/*weeks to days*/:(7.0/30.5)/*months to weeks*/;
        else {
            if (curInt > ci10Minutes)
                dis *= (curInt == ciDay)?(1.0/24.0)/*days to hours*/:(1.0/6.0)/*hours to 10mins*/;
            else
                dis *= 0.1;
        }
        curInt--;
    }
    _graphicLayer.desiredItemSize = dis;
}

- (void) resetData {
    [_graphicLayer dropMainData];
    for (UILabel *l in self.timeLabels)
        l.hidden = YES;
    for (UILabel *l in self.dateLabels)
        l.hidden = YES;
}

- (void) resetSubScaleData:(NSNumber *)subScaleId {
    [_graphicLayer dropSubData:subScaleId];
}

- (void) scrollToLast {
    if (_graphicLayer.decelerating) return;
    _graphicLayer.decelerating = dsPanAfterTouch;
    velocityX = 300;
    decelerationX = decelerationX0 = 0;
    decelerationXStartTime = CFAbsoluteTimeGetCurrent();
}

- (void) feedData:(NSArray *)rows WithColumns:(NSArray *) columns SubsId:(NSNumber *)nid Main:(BOOL) mainChartData {
    NSArray *needed = @[@"OPEN", @"CLOSE", @"HIGH", @"LOW", @"VOLUME", @"FROM"]; //@[@"open", @"close", @"high", @"low", @"value", @"begin"];
    NSUInteger idxs[6];
    for (int i = 0; i < 6; i++) {
        idxs[i] = [columns indexOfObject:needed[i]];
        if (idxs[i] == NSNotFound) {
            if (i != 4) return;
            idxs[i] = [columns indexOfObject:@"VALUE"];
            if (idxs[i] == NSNotFound)
                return;
        }
    }
    if (rows.count == 0) return;
    int dataDecimals = self.decimals;
    NSArray *close0 = ((NSArray *)rows[0])[idxs[1]];
    if (close0)
        dataDecimals = [close0[1] integerValue];
    if (mainChartData) {
        [_graphicLayer beginUpdateWithEstimatedCandleCount:rows.count ScaleId:nid Decimals:dataDecimals];
    } else {
        [_graphicLayer beginUpdateSubScale:nid Decimals:dataDecimals];
    }
    for (NSArray *row in rows) {
        NSArray *h = row[idxs[2]];
        NSArray *l = row[idxs[3]];
        NSArray *o = row[idxs[0]];
        NSArray *c = row[idxs[1]];
        NSArray *v = row[idxs[4]];
        #ifdef EMUCACHE
            [_graphicLayer feedH:h[0] L:l[0] O:o[0] C:c[0] V:v AtTime:row[idxs[5]]];
        #else
            [_graphicLayer feedH:h[0] L:l[0] O:o[0] C:c[0] V:v[0] AtTime:row[idxs[5]]];
        #endif
    }
    FeedingResult r = [_graphicLayer endUpdate];
    switch (r) {
        case frCompletelyNewData: {
            [self realignAnimated:NO];
            [self.graphicLayer unmark];
            [_graphicLayer presentWithDecimals:close0[1]];
            break;
        }
        case frNewCandlesAdded:
            [self scrollToLast];
            break;
        default:
            return;
    }
}

CGAffineTransform CGAffineTransformMakeRotationAt(CGFloat angle, CGPoint pt){
    const CGFloat fx = pt.x;
    const CGFloat fy = pt.y;
    const CGFloat fcos = cos(angle);
    const CGFloat fsin = sin(angle);
    return CGAffineTransformMake(fcos, fsin, -fsin, fcos, fx - fx * fcos + fy * fsin, fy - fx * fsin - fy * fcos);
}

- (void) rotate:(ViewRotation) r Frame:(CGRect) f Around:(CGPoint) p {
    if (r == _rotation) {
        if (CGRectEqualToRect(f, self.frame))
            return;
        [UIView animateWithDuration:0.5 animations:^{
            self.frame = f;
            [self setNeedsLayout];
        } completion:^(BOOL finished) {
            [self performSelectorOnMainThread:@selector(setNeedsDisplay) withObject:nil waitUntilDone:YES];
        }];
    } else {
        _rotation = r;
        CGFloat angle = (r == vrStraight)?0:((r == vrRight)?M_PI_2:-M_PI_2);
        [UIView animateWithDuration:0.5 animations:^{
            self.transform = CGAffineTransformMakeRotationAt(angle, p);
            self.frame = f;
        } completion:^(BOOL finished) {
            self.frame = f;
            [self setNeedsLayout];

            //[_graphicLayer checkNeededCompensation];
            //[_graphicLayer setNeedsDisplay];
        }];

    }
}

- (void) setChartType:(ScaleRepresentation)t {
    if (self.graphicLayer.type != t) {
        self.graphicLayer.type = t;
        [self setupTypeSwitchButton];
    }
}

- (ScaleRepresentation) getChartType {
    return self.graphicLayer.type;
}

- (void) setAnimated:(BOOL)v {
    animated = v;
    self.graphicLayer.animated = v;
}

- (BOOL) getAnimated {
    return animated;
}

- (void) setLineProperties:(NSDictionary *) lineProperies {
    [self.graphicLayer setGraphProperties:@{kPropertySectionGeneral: lineProperies}];
}

- (void) setAxisLineProperties:(NSDictionary *) axisLineProperties {
    [self.graphicLayer setGraphProperties:@{kPropertySectionAxes: axisLineProperties}];
}

/*- (void) setGridProperties:(NSDictionary *) gridProperies {
    [self.graphicLayer setGraphProperties:@{@"grid": gridProperies}];
}*/

- (id) getSubProperty:(NSString *)propKey InSection:(NSString *) sectionKey {
    id r = [self.graphicLayer.visualProps valueForKey:sectionKey];
    if (r)
        return [r valueForKey:propKey];
    return nil;
}

- (void) setSubProperty:(NSString *)propKey InSection:(NSString *) sectionKey Value:(id) value {
    if (value)
        [self.graphicLayer setGraphProperties:@{sectionKey: @{propKey: value}}];
}

- (void) setLineWidth:(NSNumber *)v {
    [self setSubProperty:kPropertyKeyWidth InSection:kPropertySectionGeneral Value:v];
}

- (NSNumber *) getLineWidth {
    return [self getSubProperty:kPropertyKeyWidth InSection:kPropertySectionGeneral];
}

- (void) setLineColor:(UIColor *)v {
    [self setSubProperty:kPropertyKeyColor InSection:kPropertySectionGeneral Value:v];
}

- (UIColor *) getLineColor {
    return [self getSubProperty:kPropertyKeyWidth InSection:kPropertySectionGeneral];
}

- (void) setVerticalGridColor:(UIColor *)v {
    [self setSubProperty:kPropertyKeyColor InSection:kPropertySectionVGrid Value:v];
}

- (UIColor *) getVerticalGridColor {
    return [self getSubProperty:kPropertyKeyWidth InSection:kPropertySectionVGrid];
}

- (void) setHorizontalGridColor:(UIColor *)v {
    [self setSubProperty:kPropertyKeyColor InSection:kPropertySectionHGrid Value:v];
}

- (UIColor *) getHorizontalGridColor {
    return [self getSubProperty:kPropertyKeyColor InSection:kPropertySectionHGrid];
}

- (void) setShowVerticalStripes:(BOOL)v {
    [self setSubProperty:kPropertyKeyVerticalBars InSection:kPropertySectionVGrid Value:@(v)];
}

- (BOOL) getShowVerticalStripes {
    return [[self getSubProperty:kPropertyKeyVerticalBars InSection:kPropertySectionVGrid] boolValue];
}

- (void) setStripeColor:(UIColor *)v {
    [self setSubProperty:kPropertyKeyFillColor InSection:kPropertySectionVGrid Value:v];
}

- (UIColor *) getStripeColor {
    return [self getSubProperty:kPropertyKeyFillColor InSection:kPropertySectionVGrid];
}

- (void) setVerticalGridWidth:(NSNumber *)v {
    [self setSubProperty:kPropertyKeyWidth InSection:kPropertySectionVGrid Value:v];
}

- (NSNumber *) getVerticalGridWidth {
    return [self getSubProperty:kPropertyKeyWidth InSection:kPropertySectionVGrid];
}

- (void) getHorizontalGridWidth:(NSNumber *)v {
    [self setSubProperty:kPropertyKeyColor InSection:kPropertySectionHGrid Value:v];
}

- (NSNumber *) getHorizontalGridWidth {
    return [self getSubProperty:kPropertyKeyWidth InSection:kPropertySectionHGrid];
}

- (void) setAxisWidth:(NSNumber *)v {
    [self setSubProperty:kPropertyKeyWidth InSection:kPropertySectionAxes Value:v];
    [self setNeedsLayout];
}

- (NSNumber *) getAxisWidth {
    return [self getSubProperty:kPropertyKeyWidth InSection:kPropertySectionAxes];
}

- (void) feedNews:(id) newsId At:(NSDate *)time {
//    int idx = 0;
//    NSDictionary *rec = @{@"id": newsId, @"time": time};
//    if (self.news == nil)
//        self.news = [[NSMutableArray alloc] init];
//    for (NSDictionary *newsRec in self.news)
//        if ([[newsRec valueForKey:@"id"] isEqualToString:newsId]) return;
//    idx = self.news.count;
//    [self.news addObject:rec];
////    if (![_graphicLayer feedNews:idx At:time])
////        [self.notPlacedNews addObject:rec];
}

@end


@implementation UIImage (Extras)

- (UIImage *)imageByScalingProportionallyToScale:(CGFloat) scaleFactor {
    
    UIImage *sourceImage = self;
    UIImage *newImage = nil;
    
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    CGFloat scaledWidth  = width * scaleFactor;
    CGFloat scaledHeight = height * scaleFactor;
    
    // center the image
        
    //thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
    //thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(scaledWidth, scaledHeight), NO, 2.0);
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if(newImage == nil) NSLog(@"could not scale image");
    
    
    return newImage ;
}

@end
